# SeeYourHealth

Projet de PPE JAVA : SeeYourHealth

Ce projet a pour but de permettre à des visiteurs de s'inscrire à des tests de médicaments et de voir le résultat de ces tests.

Un test est réalisé par un médecin pour un visiteur avec un médicament. Le résultat du test du médicament est actualisé par une secrétaire qui a comme référent un médecin qu'elle choisit lors de sa première connexion.

Pour la documentation technique plus détaillée, il faut utiliser les documents HTML situés dans DocumentationTechnique à la racine du projet.
