package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.hibernate.NonUniqueObjectException;

import DAO.DAOLaboratory;
import DAO.DAOMedecin;
import DAO.DAOSample;
import DAO.DAOTest;
import DefaultModelJTable.MyDefaultModelVisiteur;
import model.Laboratory;
import model.Medecin;
import model.Sample;
import model.Test;
import model.Users;
import util.HibernateUtil;
import views.Visiteur;

public class ControllerVisiteur implements ActionListener{

	private DAOTest daoTest;
	private DAOMedecin daoMedecin = new DAOMedecin(HibernateUtil.getSessionFactory().openSession());
	private DAOSample daoSample = new DAOSample(HibernateUtil.getSessionFactory().openSession());
	private DAOLaboratory daoLaboratory = new DAOLaboratory(HibernateUtil.getSessionFactory().openSession());

	private Visiteur visiteur;

	private MyDefaultModelVisiteur myDTM;

	private Users user;
	
	private List<Test> tests;
	private List<Medecin> medecins;
	private List<Laboratory> laboratories;
	private List<Sample> samples;
	private Laboratory selectedLaboratory;
	private Medecin selectedMedecin;
	private Sample selectedSample;

	public ControllerVisiteur(Users user, DAOTest daoTest, Visiteur visiteur) {
		this.user = user;
		this.daoTest = daoTest;
		this.visiteur = visiteur;
	}

	/**
	 * Init the controller on the page VoirTest
	 */
	public void init() {
		tests = daoTest.findAllByUserId(user.getId());
		laboratories = daoLaboratory.findAll();
		for (Laboratory laboratory : laboratories) {
			visiteur.getCbInscriptionLaboratory().addItem(laboratory);
		}
		selectedLaboratory = laboratories.get(0);
		medecins = laboratories.get(0).getMedecins();
		for (Medecin medecin : medecins) {
			visiteur.getCbInscriptionMedecin().addItem(medecin);
		}
		selectedMedecin = medecins.get(0);
		samples = daoSample.findAll();
		for (Sample sample : samples) {
			visiteur.getCbInscriptionMedicament().addItem(sample);
		}
		selectedSample = samples.get(0);
		visiteur.getLblInscriptionTestLaboAdresse().setText("Adresse : " + laboratories.get(0).getAdresse());
		visiteur.getLblInscriptionTestLaboRegion().setText("Region : " + laboratories.get(0).getRegion());
		visiteur.getLblInscriptionTestMedecinNom().setText("Nom : " + medecins.get(0).getLastName());
		visiteur.getLblInscriptionTestMedecinPrenom().setText("Prénom : " + medecins.get(0).getFirstName());
		visiteur.getLblInscriptionTestMedecinEmail().setText("Email : " + medecins.get(0).getEmail());
		visiteur.getLblInscriptionTestMedecinPhone().setText("Téléphone : " + medecins.get(0).getPhone());
		visiteur.getLblInscriptionTestMedicamentName().setText("Nom : " + samples.get(0).getName());
		if(samples.get(0).getDanger() != null) {
			visiteur.getLblInscriptionTestMedicamentDanger().setText("Danger potentiel : " + samples.get(0).getDanger().getName());
		} else {
			visiteur.getLblInscriptionTestMedicamentDanger().setText("Danger potentiel : Inconnu");
		}
		visiteur.getBtnVoirTestToInscriptionTest().addActionListener(this);
		visiteur.getBtnInscriptionTestToVoirTest().addActionListener(this);
		visiteur.getBtnInscriptionTestInscription().addActionListener(this);
		visiteur.getCbInscriptionLaboratory().addActionListener(this);
		visiteur.getCbInscriptionMedecin().addActionListener(this);
		visiteur.getCbInscriptionMedicament().addActionListener(this);
		visiteur.getLblConnexionStatus().setText("Connecté en tant que : Visiteur");
		visiteur.getPanelCard().removeAll();
		visiteur.getPanelCard().add(visiteur.getPanelVoirTest());
		visiteur.getPanelCard().repaint();
		visiteur.getPanelCard().revalidate();
		myDTM = new MyDefaultModelVisiteur(tests);
		visiteur.getJTableTests().setModel(myDTM);
		visiteur.setVisible(true);
	}

	/**
	 * Refresh the panel Inscription when the user select a new Laboratory
	 * @param lastMedecins the last List Medecin
	 */
	public void refreshWindowInscriptionWhenLaboSelected(List<Medecin> lastMedecins) {
		visiteur.getLblInscriptionTestMedecinNom().setText("Nom : " + medecins.get(0).getLastName());
		visiteur.getLblInscriptionTestMedecinPrenom().setText("Prénom : " + medecins.get(0).getFirstName());
		visiteur.getLblInscriptionTestMedecinEmail().setText("Email : " + medecins.get(0).getEmail());
		visiteur.getLblInscriptionTestMedecinPhone().setText("Téléphone : " + medecins.get(0).getPhone());
		visiteur.getLblInscriptionTestLaboAdresse().setText("Adresse : " + selectedLaboratory.getAdresse());
		visiteur.getLblInscriptionTestLaboRegion().setText("Région : " + selectedLaboratory.getRegion());
		selectedMedecin = medecins.get(0);
		for (Medecin medecin : medecins) {
			visiteur.getCbInscriptionMedecin().addItem(medecin);
		}
		int i = 0;
		while(i < lastMedecins.size()) {
			visiteur.getCbInscriptionMedecin().removeItemAt(0);
			i++;
		}
	}

	/**
	 * Refresh the panel Inscription when the user select a new Medecin
	 */
	public void refreshWindowInscriptionWhenMedecinSelected(){
		for (Medecin medecin : medecins) {
			if(medecin.getId() == selectedMedecin.getId()) {
				visiteur.getLblInscriptionTestMedecinNom().setText("Nom : " + medecin.getLastName());
				visiteur.getLblInscriptionTestMedecinPrenom().setText("Prénom : " + medecin.getFirstName());
				visiteur.getLblInscriptionTestMedecinEmail().setText("Email : " + medecin.getEmail());
				visiteur.getLblInscriptionTestMedecinPhone().setText("Téléphone : " + medecin.getPhone());
				break;
			}
		}
	}

	/**
	 * Refresh the panel Inscription when the user select a new Sample
	 */
	public void refreshWindowInscriptionWhenMedicamentSelected() {
		for (Sample sample : samples) {
			if(sample.getId() == selectedSample.getId()) {
				visiteur.getLblInscriptionTestMedicamentName().setText("Nom : " + sample.getName());
				if(sample.getDanger() != null) {
					visiteur.getLblInscriptionTestMedicamentDanger().setText("Danger potentiel : " + sample.getDanger().getName());
				} else {
					visiteur.getLblInscriptionTestMedicamentDanger().setText("Danger potentiel : Inconnu");
				}
				break;
			}
		}
	}

	/**
	 * Switch panel from VoirTest to InscriptionTest
	 */
	public void goToInscription() {
		visiteur.getPanelCard().removeAll();
		visiteur.getPanelCard().add(visiteur.getPanelInscriptionTest());
		visiteur.getPanelCard().repaint();
		visiteur.getPanelCard().revalidate();
	}

	/**
	 * Switch panel from InscriptionTest to VoirTest
	 */
	public void goToVoirTest() {
		visiteur.getPanelCard().removeAll();
		visiteur.getPanelCard().add(visiteur.getPanelVoirTest());
		visiteur.getPanelCard().repaint();
		visiteur.getPanelCard().revalidate();
	}

	/**
	 * Insert in the Database a new Test for the current user
	 */
	public void doInscriptionTest() {
		Test newTest = new Test();
		newTest.setMedecin(selectedMedecin);
		newTest.setResult("En cours de traitement");
		newTest.setSample(selectedSample);
		newTest.setUser(user);
		try {
			daoTest.saveOrUpdate(newTest);
			tests.add(newTest);
			myDTM.setTests(tests);
			JOptionPane.showMessageDialog(visiteur,
					"Votre test a bien été pris en compte",
					"Succès",
					JOptionPane.PLAIN_MESSAGE);
		} catch (NonUniqueObjectException e) {
			daoTest.resetTransaction();
			JOptionPane.showMessageDialog(visiteur,
					"Vous avez déjà un test avec le médicament : " + selectedSample.getName(),
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Router of the actions
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch (btn.getName()) {
			case "btnVoirTestToInscriptionTest":
				goToInscription();
				break;
			case "btnInscriptionTestToVoirTest":
				goToVoirTest();
				break;
			case "btnInscriptionTestInscription":
				doInscriptionTest();
				break;
			default:
				break;
			}
		} else if (evt.getSource() instanceof JComboBox) {
			JComboBox cb = (JComboBox) evt.getSource();
			switch (cb.getName()) {
			case "cbInscriptionLaboratory":
				Laboratory labo = (Laboratory) cb.getSelectedItem();
				List<Medecin> lastMedecins = medecins;
				medecins = labo.getMedecins();
				selectedLaboratory = labo;
				refreshWindowInscriptionWhenLaboSelected(lastMedecins);
				break;
			case "cbInscriptionMedecin":
				Medecin med = (Medecin) cb.getSelectedItem();
				selectedMedecin = med;
				refreshWindowInscriptionWhenMedecinSelected();
				break;
			case "cbInscriptionMedicament":
				Sample sample = (Sample) cb.getSelectedItem();
				selectedSample = sample;
				refreshWindowInscriptionWhenMedicamentSelected();
				break;
			default:
				break;
			}
		}

	}

}
