package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.TableColumn;

import DAO.DAODanger;
import DAO.DAORights;
import DAO.DAOSample;
import DAO.DAOUser;
import DAO.DAOUserRight;
import DefaultModelJTable.MyDefaultModelAdminUsers;
import model.Danger;
import model.Laboratory;
import model.Medecin;
import model.Rights;
import model.Sample;
import model.UserRight;
import model.Users;
import util.HibernateUtil;
import views.Admin;

public class ControllerAdmin implements ActionListener{

	private DAOUser daoUser;
	private DAOUserRight daoUserRight;
	private DAORights daoRights;
	private DAODanger daoDanger;
	private DAOSample daoSample;

	private Admin admin;

	private MyDefaultModelAdminUsers MyDTMUsers;

	private List<Users> users;
	private List<UserRight> userRights;
	private List<Rights> rights;
	private List<Danger> dangers;
	private List<Sample> samples;

	private Users selectedUser;
	private Rights selectedRight;
	private Danger selectedAjouterDanger;
	private Danger selectedModifDanger;
	private Sample selectedSample;

	public ControllerAdmin(Admin admin, DAOUser daoUser, DAOUserRight daoUserRight) {
		this.admin = admin;
		this.daoUser = daoUser;
		this.daoUserRight = daoUserRight;
	}

	/**
	 * Init the controller on the page AdminChangeUserRights
	 */
	public void init() {
		daoSample = new DAOSample(HibernateUtil.getSessionFactory().openSession());
		daoDanger = new DAODanger(HibernateUtil.getSessionFactory().openSession());
		daoRights = new DAORights(HibernateUtil.getSessionFactory().openSession());
		rights = daoRights.findAll();
		selectedRight = rights.get(0);
		for (Rights right : rights) {
			admin.getCbAdminRights().addItem(right);
		}
		users = daoUser.findAll();
		selectedUser = users.get(0);
		refreshUser();
		MyDTMUsers = new MyDefaultModelAdminUsers(userRights);
		admin.getJTableUserRights().setModel(MyDTMUsers);
		for (Users user : users) {
			admin.getCbUsers().addItem(user);
		}
		samples = daoSample.findAll();
		for (Sample sample : samples) {
			admin.getCbModifMedicamentMedicament().addItem(sample);
		}
		selectedSample = samples.get(0);
		selectedModifDanger = selectedSample.getDanger();
		dangers = daoDanger.findAll();
		for (Danger danger : dangers) {
			admin.getCbMedocAjouterDanger().addItem(danger);
			admin.getCbModifMedicamentDanger().addItem(danger);
			if(danger.getId() == selectedSample.getDanger().getId()) {
				admin.getCbModifMedicamentDanger().setSelectedItem(danger);
			}
		}
		selectedAjouterDanger = dangers.get(0);
		admin.getCbUsers().addActionListener(this);
		admin.getCbAdminRights().addActionListener(this);
		admin.getBtnAdminAjouter().addActionListener(this);
		admin.getBtnAdminSupprimer().addActionListener(this);
		admin.getBtnModifierMedicament().addActionListener(this);
		admin.getBtnAjouterMedicament().addActionListener(this);
		admin.getBtnModifierRoles().addActionListener(this);
		admin.getBtnMedocAjouterAjouter().addActionListener(this);
		admin.getCbMedocAjouterDanger().addActionListener(this);
		admin.getCbModifMedicamentMedicament().addActionListener(this);
		admin.getCbModifMedicamentDanger().addActionListener(this);
		admin.getBtnModifMedicamentModifier().addActionListener(this);
		admin.getLblConnexionStatus().setText("Connecté en tant que : Admin");
		goToModifRole();
		admin.setVisible(true);
	}

	/**
	 * Refresh the label for the user
	 */
	public void refreshUser() {
		admin.getLblUserEmail().setText("Email : " + selectedUser.getEmail());
		admin.getLblUserFirstName().setText("Prénom : " + selectedUser.getFirstName());
		admin.getLblUserLastName().setText("Nom : " + selectedUser.getLastName());
		userRights = daoUserRight.findAllByUserId(selectedUser.getId());
	}

	/**
	 * Refresh the JTable component
	 */
	public void refreshJTable() {
		userRights = daoUserRight.findAllByUserId(selectedUser.getId());
		MyDTMUsers.setUserRights(userRights);
		MyDTMUsers.fireTableDataChanged();
	}

	/**
	 * Add to the database the right choose for the user choose
	 */
	public void addRightToUser() {
		UserRight newUserRight = new UserRight();
		newUserRight.setUser(selectedUser);
		newUserRight.setRight(selectedRight);
		try {
			daoUserRight.saveOrUpdate(newUserRight);
			userRights.add(newUserRight);
			MyDTMUsers.setUserRights(userRights);
			MyDTMUsers.fireTableDataChanged();
			JOptionPane.showMessageDialog(admin,
					"Le rôle : " + selectedRight.getName() + " a bien été ajouté à l'utilisateur : " + selectedUser.getLastName() + " " + selectedUser.getFirstName() ,
					"Succès",
					JOptionPane.PLAIN_MESSAGE);
		} catch (Exception e) {
			daoUserRight.resetTransaction();
			JOptionPane.showMessageDialog(admin,
					"L'utilisateur : " + selectedUser.getLastName() + " " + selectedUser.getFirstName() + " a déjà le rôle : " + selectedRight.getName(),
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Delete from the database the right choose for the user choose
	 */
	public void suppRightToUser() {
		int selectedRow = admin.getJTableUserRights().getSelectedRow();
		if(selectedRow >= 0) {
			if(userRights.size() <= 1) {
				JOptionPane.showMessageDialog(admin,
						"Vous ne pouvez pas supprimer tous les rôles d'un utilisateur.",
						"Erreur",
						JOptionPane.ERROR_MESSAGE);
			}
			else {
				UserRight userRightToSupp = userRights.get(selectedRow);
				try {
					daoUserRight.delete(userRightToSupp);
					userRights.remove(selectedRow);
					MyDTMUsers.setUserRights(userRights);
					MyDTMUsers.fireTableDataChanged();
					JOptionPane.showMessageDialog(admin,
							"Le rôle : " + userRightToSupp.getRight().getName() + " a bien été retiré à l'utilisateur : " + selectedUser.getLastName() + " " + selectedUser.getFirstName(),
							"Erreur",
							JOptionPane.PLAIN_MESSAGE);
				} catch (Exception e) {
					daoUserRight.resetTransaction();
					JOptionPane.showMessageDialog(admin,
							"Une erreur est survenue lors de la suppression du rôle.",
							"Erreur",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		} else {
			JOptionPane.showMessageDialog(admin,
					"Vous devez sélectionner une ligne du tableau.",
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Display the panel ModifMedicament
	 */
	public void goToModifMedicament() {
		admin.getPanelCardAdmin().removeAll();
		admin.getPanelCardAdmin().add(admin.getPanelAdminModifMedoc());
		admin.getPanelCardAdmin().repaint();
		admin.getPanelCardAdmin().revalidate();
	}

	/**
	 * Display the panel ModifRole
	 */
	public void goToModifRole() {
		admin.getPanelCardAdmin().removeAll();
		admin.getPanelCardAdmin().add(admin.getPanelAdminChangeUserRights());
		admin.getPanelCardAdmin().repaint();
		admin.getPanelCardAdmin().revalidate();
	}

	/**
	 * Display the panel AjouterMedoc
	 */
	public void goToAjouterMedoc() {
		admin.getPanelCardAdmin().removeAll();
		admin.getPanelCardAdmin().add(admin.getPanelAdminAjouterMedoc());
		admin.getPanelCardAdmin().repaint();
		admin.getPanelCardAdmin().revalidate();
	}

	/**
	 * Add a Sample in the database
	 */
	public void doAddMedoc(){
		if(!admin.getTxtMedocAjouterName().getText().equals("")) {
			Sample newSample = new Sample();
			newSample.setDanger(selectedAjouterDanger);
			newSample.setName(admin.getTxtMedocAjouterName().getText());
			try {
				daoSample.saveOrUpdate(newSample);
				JOptionPane.showMessageDialog(admin,
						"Le médicament a bien été ajouté.",
						"Succes",
						JOptionPane.PLAIN_MESSAGE);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(admin,
						"Une erreur est survenue.",
						"Erreur",
						JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(admin,
					"Tous les champs doivent être remplis.",
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public void doModifSample() {
		selectedSample.setDanger(selectedModifDanger);
		try {
			daoSample.saveOrUpdate(selectedSample);
			JOptionPane.showMessageDialog(admin,
					"Le médicament a bien été modifié.",
					"Succes",
					JOptionPane.PLAIN_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(admin,
					"Une erreur est survenue lors de la modification du médicament.",
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Router of the actions
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch (btn.getName()) {
			case "btnAdminAjouter":
				addRightToUser();
				break;
			case "btnAdminSupprimer":
				suppRightToUser();
				break;
			case "btnModifierMedicament":
				goToModifMedicament();
				break;
			case "btnModifierRoles":
				goToModifRole();
				break;
			case "btnAjouterMedicament":
				goToAjouterMedoc();
				break;
			case "btnMedocAjouterAjouter":
				doAddMedoc();
				break;
			case "btnModifMedicamentModifier":
				doModifSample();
				break;
			default:
				break;
			}
		} else if (evt.getSource() instanceof JComboBox) {
			JComboBox cb = (JComboBox) evt.getSource();
			switch (cb.getName()) {
			case "cbUsers":
				selectedUser = (Users) cb.getSelectedItem();
				refreshUser();
				refreshJTable();
				break;
			case "cbAdminRights":
				selectedRight = (Rights) cb.getSelectedItem();
				break;
			case "cbMedocAjouterDanger":
				selectedAjouterDanger = (Danger) cb.getSelectedItem();
				break;
			case "cbModifMedicamentMedicament":
				selectedSample = (Sample) cb.getSelectedItem();
				for (int i = 0; i < admin.getCbModifMedicamentDanger().getItemCount(); i++) {
					Danger dangerTempo = (Danger)admin.getCbModifMedicamentDanger().getItemAt(i);
					if(dangerTempo.getId() == selectedSample.getDanger().getId()) {
						admin.getCbModifMedicamentDanger().setSelectedIndex(i);
					}
				}
				break;
			case "cbModifMedicamentDanger":
				selectedModifDanger = (Danger) cb.getSelectedItem();
				break;
			default:
				break;
			}
		}
	}

}
