package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import DAO.DAOLaboratory;
import DAO.DAOMedecin;
import DAO.DAOTest;
import DAO.DAOUser;
import DefaultModelJTable.MyDefaultModelSecretaire;
import DefaultModelJTable.MyDefaultModelSecretaireTrie;
import javassist.expr.Instanceof;
import model.Laboratory;
import model.Medecin;
import model.Sample;
import model.Test;
import model.Users;
import util.HibernateUtil;
import views.Secretaire;

public class ControllerSecretaire implements ActionListener{

	private Users user;

	private DAOTest daoTest;
	private DAOUser daoUser;
	private DAOMedecin daoMedecin;
	private DAOLaboratory daoLaboratory;

	private MyDefaultModelSecretaire myDTM;
	private MyDefaultModelSecretaireTrie myDTMTrie;

	private Secretaire secretaire;

	private List<Test> tests;
	private List<Test> testsTrie;
	private Medecin medecinRef;
	private List<Laboratory> laboratories;
	private List<Medecin> medecins;
	private String medicament;

	public ControllerSecretaire(Users user, DAOTest daoTest, Secretaire secretaire) {
		this.user = user;
		this.daoTest = daoTest;
		this.secretaire = secretaire;
	}

	public void init() {
		if(user.getMedecin() == null) {
			daoUser = new DAOUser(HibernateUtil.getSessionFactory().openSession());
			daoLaboratory = new DAOLaboratory(HibernateUtil.getSessionFactory().openSession());
			laboratories = daoLaboratory.findAll();
			for (Laboratory laboratory : laboratories) {
				secretaire.getCbFirstConnexionLaboratory().addItem(laboratory);
			}
			medecins = laboratories.get(0).getMedecins();
			medecinRef = medecins.get(0);
			for (Medecin medecin : medecins) {
				secretaire.getCbFirstConnexionMedecin().addItem(medecin);
			}
			secretaire.getLblMedRef().setText("Médecin référent : Aucun");
			secretaire.getLblFirstConnexionLaboAdresse().setText("Adresse : " + laboratories.get(0).getAdresse());
			secretaire.getLblFirstConnexionLaboRegion().setText("Région : " + laboratories.get(0).getRegion());
			secretaire.getLblFirstConnexionMedecinNom().setText("Nom : " + medecins.get(0).getLastName());
			secretaire.getLblFirstConnexionMedecinPrenom().setText("Prenom : " + medecins.get(0).getFirstName());
			secretaire.getLblFirstConnexionMedecinEmail().setText("Email : " + medecins.get(0).getEmail());
			secretaire.getLblFirstConnexionMedecinPhone().setText("Téléphone : " + medecins.get(0).getPhone());
			secretaire.getBtnFirstConnexionChoose().addActionListener(this);
			secretaire.getCbFirstConnexionLaboratory().addActionListener(this);
			secretaire.getCbFirstConnexionMedecin().addActionListener(this);
			secretaire.getBtnTousLesTests().addActionListener(this);
			secretaire.getBtnTrierParMdicament().addActionListener(this);
			secretaire.getCbMedicament().addActionListener(this);
			secretaire.getPanelCard().removeAll();
			secretaire.getPanelCard().add(secretaire.getPanelFirstConnexion());
			secretaire.getPanelCard().repaint();
			secretaire.getPanelCard().revalidate();
		}
		else {
			medecinRef = user.getMedecin();
			secretaire.getLblMedRef().setText("Médecin référent : " + medecinRef.getLastName() + " " + medecinRef.getFirstName());
			secretaire.getBtnGererMedecinModifier().addActionListener(this);
			secretaire.getBtnTousLesTests().addActionListener(this);
			secretaire.getBtnTrierParMdicament().addActionListener(this);
			secretaire.getCbMedicament().addActionListener(this);
			tests = daoTest.findAllByMedecinId(medecinRef.getId());
			testsTrie = tests;
			for (Test test : tests) {
				String name = test.getSample().getName();
				secretaire.getCbMedicament().addItem(test.getSample());
			}
			myDTM = new MyDefaultModelSecretaire(tests);
			secretaire.getJTableGererMedecin().setModel(myDTM);
			myDTMTrie = new MyDefaultModelSecretaireTrie(tests);
			secretaire.getJTableTrierMedicament().setModel(myDTMTrie);
			secretaire.getPanelCard().removeAll();
			secretaire.getPanelCard().add(secretaire.getPanelGererMedecin());
			secretaire.getPanelCard().repaint();
			secretaire.getPanelCard().revalidate();
		}
		secretaire.getLblConnexionStatus().setText("Connecté en tant que : Secretaire");
		secretaire.setVisible(true);
	}

	public void chooseMedecin() {
		user.setMedecin(medecinRef);
		try {
			daoUser.saveOrUpdate(user);
			JOptionPane.showMessageDialog(secretaire,
					"Votre médecin référent a bien été pris en compte.",
					"Succès",
					JOptionPane.PLAIN_MESSAGE);
		} catch (Exception e) {
			daoUser.resetTransaction();
			System.out.println(e);
			JOptionPane.showMessageDialog(secretaire,
					"Une erreur est survenue.",
					"Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Refresh the panel FirstConnexion when the user select a new Medecin
	 */
	public void refreshWindowInscriptionWhenMedecinSelected(){
		for (Medecin medecin : medecins) {
			if(medecin.getId() == medecinRef.getId()) {
				secretaire.getLblFirstConnexionMedecinNom().setText("Nom : " + medecin.getLastName());
				secretaire.getLblFirstConnexionMedecinPrenom().setText("Prénom : " + medecin.getFirstName());
				secretaire.getLblFirstConnexionMedecinEmail().setText("Email : " + medecin.getEmail());
				secretaire.getLblFirstConnexionMedecinPhone().setText("Téléphone : " + medecin.getPhone());
				break;
			}
		}
	}

	/**
	 * Refresh the panel FirstConnexion when the user select a new Laboratory
	 * @param lastMedecins the last List Medecin
	 * @param newLabo the new laboratory selected
	 */
	public void refreshWindowInscriptionWhenLaboSelected(List<Medecin> lastMedecins, Laboratory newLabo) {
		secretaire.getLblFirstConnexionMedecinNom().setText("Nom : " + medecins.get(0).getLastName());
		secretaire.getLblFirstConnexionMedecinPrenom().setText("Prénom : " + medecins.get(0).getFirstName());
		secretaire.getLblFirstConnexionMedecinEmail().setText("Email : " + medecins.get(0).getEmail());
		secretaire.getLblFirstConnexionMedecinPhone().setText("Téléphone : " + medecins.get(0).getPhone());
		secretaire.getLblFirstConnexionLaboAdresse().setText("Adresse : " + newLabo.getAdresse());
		secretaire.getLblFirstConnexionLaboRegion().setText("Région : " + newLabo.getRegion());
		medecinRef = medecins.get(0);
		for (Medecin medecin : medecins) {
			secretaire.getCbFirstConnexionMedecin().addItem(medecin);
		}
		int i = 0;
		while(i < lastMedecins.size()) {
			secretaire.getCbFirstConnexionMedecin().removeItemAt(0);
			i++;
		}
	}

	/**
	 * Modification of a Test when the button btnGererMedecinModifier is pressed
	 */
	public void doModifTest() {
		HashSet<Test> tests = myDTM.getModifiedTests();
		for (Test test : tests) {
			try {
				daoTest.saveOrUpdate(test);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(secretaire, "Une erreur s'est produite lors de la modification des données.",
						"Erreur", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
		JOptionPane.showMessageDialog(secretaire, "Toutes les modifications ont été prises en compte.",
				"Succès", JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Display the view Connexion
	 */
	public void trie() {
		secretaire.getPanelCard().removeAll();
		secretaire.getPanelCard().add(secretaire.getPanelTrierMedicament());
		secretaire.getPanelCard().repaint();
		secretaire.getPanelCard().revalidate();
	}
	
	/**
	 * Display the view Connexion
	 */
	public void allTests() {
		secretaire.getPanelCard().removeAll();
		secretaire.getPanelCard().add(secretaire.getPanelGererMedecin());
		secretaire.getPanelCard().repaint();
		secretaire.getPanelCard().revalidate();
	}
	
	/**
	 * Get a list of tests by Sample
	 */
	public void chooseMedicament() {
		/*
		int i = 0;
		while(i < testsTrie.size()) {
			secretaire.getCbMedicament().removeItemAt(0);
			i++;
		}
		for (Test test : tests) {
			if(test.getSample().getName() == medicament) {
				testsTrie.add(test);
			}
		}
		myDTMTrie = new MyDefaultModelSecretaireTrie(testsTrie);
		secretaire.getJTableTrierMedicament().setModel(myDTMTrie);
		*/
	}

	/**
	 * Router of the actions
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch (btn.getName()) {
			case "btnFirstConnexionChoose":
				chooseMedecin();
				break;
			case "btnGererMedecinModifier":
				doModifTest();
				break;
			case "btnTrie":
				trie();
				break;
			case "btnAllTests":
				allTests();
				break;
			default:
				break;
			}
		} else if (evt.getSource() instanceof JComboBox) {
			JComboBox cb = (JComboBox) evt.getSource();
			switch (cb.getName()) {
			case "cbFirstConnexionLaboratory":
				Laboratory labo = (Laboratory) cb.getSelectedItem();
				List<Medecin> lastMedecins = medecins;
				medecins = labo.getMedecins();
				refreshWindowInscriptionWhenLaboSelected(lastMedecins, labo);
				break;
			case "cbFirstConnexionMedecin":
				Medecin med = (Medecin) cb.getSelectedItem();
				medecinRef = med;
				refreshWindowInscriptionWhenMedecinSelected();
				break;
			case "cbMedicament":
				Sample sample = (Sample) cb.getSelectedItem();
				medicament = sample.getName();
				chooseMedicament();
				break;
			default:
				break;
			}
		}
	}

}
