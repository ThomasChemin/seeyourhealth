package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;

import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.internal.SQLExceptionTypeDelegate;

import DAO.DAOMedecin;
import DAO.DAORights;
import DAO.DAOTest;
import DAO.DAOUser;
import DAO.DAOUserRight;
import model.Medecin;
import model.Rights;
import model.Test;
import model.Users;
import model.UserRight;
import util.AdresseVisitor;
import util.CityVisitor;
import util.EmailVisitor;
import util.Filter;
import util.FirstNameVisitor;
import util.HibernateUtil;
import util.LastNameVisitor;
import util.PasswordVisitor;
import util.PhoneVisitor;
import util.hashPassword;
import views.Admin;
import views.Connexion;
import views.Secretaire;
import views.Visiteur;

public class ControllerConnexion implements ActionListener {
	private DAOUser daoUser;
	private DAORights daoRight;
	private DAOUserRight daoUserRight;
	private Connexion connexion;

	private String mailUser;
	private String passwordUser;
	private List<UserRight> userRights;
	private Rights userRightChoose;
	private Users user;

	private String lastName;
	private String firstName;
	private String adresse;
	private String city;
	private String phone;
	private String email;
	private String password;
	private String passwordVerify;

	public ControllerConnexion(Connexion c, DAOUser user, DAORights right, DAOUserRight userRight) {
		daoUser = user;
		daoRight = right;
		daoUserRight = userRight;
		connexion = c;
	}

	/**
	 * Init the page on the view Accueil
	 */
	public void init() {
		connexion.getBtnConnexion().addActionListener(this);
		connexion.getBtnInscription().addActionListener(this);
		connexion.getBtnConnexionInscription().addActionListener(this);
		connexion.getBtnInscriptionConnexion().addActionListener(this);
		connexion.getTxtConnexionMail().addActionListener(this);
		connexion.getPswConnexionPassword().addActionListener(this);
		connexion.getBtnConnexionConnexion().addActionListener(this);
		connexion.getBtnInscriptionInscription().addActionListener(this);
		connexion.getCbConnexionChooseRight().addActionListener(this);
		connexion.getBtnConnexionChooseRight().addActionListener(this);
		connexion.getPanelBase().removeAll();
		connexion.getPanelBase().add(connexion.getPanelAccueil());
		connexion.getPanelBase().repaint();
		connexion.getPanelBase().revalidate();
		connexion.setVisible(true);
	}

	/**
	 * Display the view Connexion
	 */
	public void connexion() {
		connexion.getPanelBase().removeAll();
		connexion.getPanelBase().add(connexion.getPanelConnexion());
		connexion.getPanelBase().repaint();
		connexion.getPanelBase().revalidate();
	}

	/**
	 * Log the user if the loggins are correct
	 */
	public void doConnexion() {
		passwordUser = connexion.getPswConnexionPassword().getText();
		try {
			passwordUser = hashPassword.hash(passwordUser);
		} catch (NoSuchAlgorithmException e) {
			connexion.getLblConnexionErrors().setText(errors("badHash"));
			e.printStackTrace();
		}
		mailUser = connexion.getTxtConnexionMail().getText();
		try {
			user = daoUser.findByMailAndPassword(mailUser, passwordUser);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (user == null) {
			connexion.getLblConnexionErrors().setText(errors("ID"));
			connexion.getPswConnexionPassword().setText("");
		} else {
			userRights = user.getUserRights();
			if(userRights.size() == 1) {
				connexion.setVisible(false);
				if(user.getUserRights().get(0).getRight().getId() == 2) {
					new ControllerVisiteur(user, 
							new DAOTest(HibernateUtil.getSessionFactory().openSession()), 
							new Visiteur()).init();
				}
				else if(user.getUserRights().get(0).getRight().getId() == 3) {
					daoUser.closeSession();
					new ControllerSecretaire(user, 
							new DAOTest(HibernateUtil.getSessionFactory().openSession()), 
							new Secretaire()).init();
				}
				else if (user.getUserRights().get(0).getRight().getId() == 1) {
					daoUser.closeSession();
					new ControllerAdmin(new Admin(), 
							new DAOUser(HibernateUtil.getSessionFactory().openSession()),
							new DAOUserRight(HibernateUtil.getSessionFactory().openSession())).init();
				}
			} else {
				for (UserRight userRight : userRights) {
					connexion.getCbConnexionChooseRight().addItem(userRight.getRight());
				}
				connexion.getBtnConnexionConnexion().setEnabled(false);
				userRightChoose = userRights.get(0).getRight();
				connexion.getCbConnexionChooseRight().setVisible(true);
				connexion.getBtnConnexionChooseRight().setVisible(true);
			}
		}
	}
	
	/**
	 * Log the user if the loggins are correct with the Right he had chosen
	 */
	public void validateConnexionWithRightChoosen(){
		connexion.setVisible(false);
		if(userRightChoose.getId() == 2) {
			new ControllerVisiteur(user, 
					new DAOTest(HibernateUtil.getSessionFactory().openSession()), 
					new Visiteur()).init();
		}
		else if(userRightChoose.getId() == 3) {
			daoUser.closeSession();
			new ControllerSecretaire(user, 
					new DAOTest(HibernateUtil.getSessionFactory().openSession()), 
					new Secretaire()).init();
		}
		else if (userRightChoose.getId() == 1) {
			daoUser.closeSession();
			new ControllerAdmin(new Admin(), 
					new DAOUser(HibernateUtil.getSessionFactory().openSession()),
					new DAOUserRight(HibernateUtil.getSessionFactory().openSession())).init();
		}
	}
	
	/**
	 * Display the view Inscription
	 */
	public void inscription() {
		connexion.getPanelBase().removeAll();
		connexion.getPanelBase().add(connexion.getPanelInscription());
		connexion.getPanelBase().repaint();
		connexion.getPanelBase().revalidate();
	}

	/**
	 * Get all the fields of Inscription and add a new User
	 */
	public void doInscription() {
		Map<String, String> toFilter = new HashMap<String, String>();
		lastName = connexion.getTxtInscriptionLastName().getText();
		firstName = connexion.getTxtIncriptionFirstName().getText();
		adresse = connexion.getTxtInscriptionAddresse().getText();
		city = connexion.getTxtInscriptionCity().getText();
		phone = connexion.getTxtInscriptionPhone().getText();
		email = connexion.getTxtInscriptionEmail().getText();
		password = connexion.getPswInscriptionPassword().getText();
		passwordVerify = connexion.getPswInscriptionPasswordConfirmation().getText();
		toFilter.put("lastName", lastName);
		toFilter.put("firstName", firstName);
		toFilter.put("addresse", adresse);
		toFilter.put("city", city);
		toFilter.put("phone", phone);
		toFilter.put("email", email);
		toFilter.put("password", password);
		toFilter.put("passwordVerify", passwordVerify);
		boolean emptyField = false;
		for (Map.Entry<String, String> data : toFilter.entrySet()) {
			if(data.getValue() == null || data.getValue().equals("")) {
				emptyField = true;
			}
		}
		if(!emptyField) {
			Filter filter = new Filter(toFilter);
			filter.acceptVisitor("lastName", new LastNameVisitor());
			filter.acceptVisitor("firstName", new FirstNameVisitor());
			filter.acceptVisitor("addresse", new AdresseVisitor());
			filter.acceptVisitor("city", new CityVisitor());
			filter.acceptVisitor("phone", new PhoneVisitor());
			filter.acceptVisitor("email", new EmailVisitor());
			filter.acceptVisitor("password", new PasswordVisitor());
			if(password.length() >= 6) {
				if (password.equals(passwordVerify)) {
					if(filter.visit()) {
						Users newUser = new Users();
						newUser.setFirstName(firstName);
						newUser.setLastName(lastName);
						newUser.setAdresse(adresse);
						newUser.setCity(city);
						newUser.setEmail(email);
						newUser.setPhone(phone);
						try {
							password = hashPassword.hash(password);
						} catch (NoSuchAlgorithmException e1) {
							connexion.getLblInscriptionerrors().setText(errors("badHash"));
							e1.printStackTrace();
						}
						newUser.setPassword(password);
						try {
							daoUser.saveOrUpdate(newUser);
							Users u = daoUser.findByMailAndPassword(email, password);
							UserRight ur = new UserRight();
							ur.setUser(u);
							Rights r = daoRight.findByName("Visiteur");
							ur.setRight(r);
							daoUserRight.saveOrUpdate(ur);
							successInscription();
						} catch (ConstraintViolationException|StackOverflowError e) {
							connexion.getLblInscriptionerrors().setText(errors("emailOrPasswordExist"));
						} catch (NoSuchAlgorithmException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						connexion.getLblInscriptionerrors().setText(errors("fieldMustBeCorrect"));
					}
				} else {
					connexion.getLblInscriptionerrors().setText(errors("passwordVerify"));
					resetInscriptionPassword();
				}
			} else {
				connexion.getLblInscriptionerrors().setText(errors("passwordLenght"));
				resetInscriptionPassword();
			} 
		} else {
			connexion.getLblInscriptionerrors().setText(errors("empty"));
		}
	}

	/**
	 * Display a success message
	 */
	public void successInscription() {
		resetInscriptionField();
		connexion.getLblInscriptionSuccess().setText("Votre compte a bien été créé.");
	}

	/**
	 * Set the Connexion's input field to null
	 */
	public void resetConnexionField() {
		connexion.getLblConnexionErrors().setText("");
		connexion.getPswConnexionPassword().setText("");
	}

	/**
	 * Reset fields of Inscription
	 */
	public void resetInscriptionField() {
		connexion.getTxtIncriptionFirstName().setText("");
		connexion.getTxtInscriptionAddresse().setText("");
		connexion.getTxtInscriptionCity().setText("");
		connexion.getTxtInscriptionEmail().setText("");
		connexion.getTxtInscriptionLastName().setText("");
		connexion.getTxtInscriptionPhone().setText("");
		connexion.getLblInscriptionerrors().setText("");
		resetInscriptionPassword();
	}

	/**
	 * Reset passwords fields of Inscription
	 */
	public void resetInscriptionPassword() {
		connexion.getPswInscriptionPassword().setText("");
		connexion.getPswInscriptionPasswordConfirmation().setText("");
	}

	/**
	 * Error messages
	 * 
	 * @param key the key of the error message
	 * @return string : the message that refere to the key
	 */
	public String errors(String key) {
		Map<String, String> errors = new HashMap<String, String>();
		errors.put("ID", "Les identifiants saisis ne sont pas les bons.");
		errors.put("passwordVerify", "Le mot de passe de confirmation ne correspond pas au mot de passe saisi.");
		errors.put("passwordLenght", "Le mot de passe doit faire plus de 6 caractères.");
		errors.put("empty", "Tous les champs doivent être remplis.");
		errors.put("fieldMustBeCorrect", "Tous les champs doivent être remplis selon le respect des critères.");
		errors.put("badHash", "Une erreur est survenue, veillez réessayer.");
		errors.put("emailOrPasswordExist", "L'addresse Email ou le mot de passe saisie existe déjà.");
		return errors.get(key);
	}

	/**
	 * Router for the actions on button
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch (btn.getName()) {
			case "Connexion":
				connexion();
				break;
			case "Inscription":
				inscription();
				break;
			case "btnConnexionInscription":
				resetConnexionField();
				inscription();
				break;
			case "btnInscriptionConnexion":
				resetInscriptionField();
				connexion();
				break;
			case "btnConnexionConnexion":
				doConnexion();
				break;
			case "btnInscriptionInscription":
				doInscription();
				break;
			case "btnConnexionChooseRight":
				validateConnexionWithRightChoosen();
				break;
			default:
				break;
			}
		}
		else if (evt.getSource() instanceof JComboBox) {
			JComboBox cb = (JComboBox) evt.getSource();
			switch (cb.getName()) {
			case "cbConnexionChooseRight":
				userRightChoose = (Rights) cb.getSelectedItem();
				break;
			default:
				break;
			}
		}
	}
}