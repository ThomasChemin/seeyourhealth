package util;

import java.util.regex.*;

public class AdresseVisitor extends AbstractVisitor {

	/**
	 * Filter adresse
	 */
	@Override
	public boolean Visit(String adresse) {
		Pattern patternFirst = Pattern.compile("[0-9]+ [a-z]+(?![-!?;.$£^*µù%])");
		Matcher matcherFirst = patternFirst.matcher(adresse);
		if(matcherFirst.find()) {
			return true;
		}
		return false;
	}

}
