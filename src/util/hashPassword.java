package util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class hashPassword {

	public String password;

	public hashPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Hash a password
	 * @param password : the password that need to be hashed
	 * @return hashedPassword : the password hashed
	 * @throws NoSuchAlgorithmException throw when a particular cryptographic algorithm is requested but is not available in the environment.
	 */
	public static String hash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[]hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

       //bytes to hex
        StringBuilder hashedPassword = new StringBuilder();
        for (byte b : hashInBytes) {
        	hashedPassword.append(String.format("%02x", b));
        }
		return hashedPassword.toString();
	}
	
}
