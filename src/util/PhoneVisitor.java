package util;

import java.util.regex.*;

public class PhoneVisitor extends AbstractVisitor {

	/**
	 * Filter for phone
	 */
	@Override
	public boolean Visit(String phone) {
		Pattern patternFirst = Pattern.compile("^[0]{1}[0-9]{9}(?![-!?;.$£^*µù% 0-9])");
		Matcher matcherFirst = patternFirst.matcher(phone);
		if(matcherFirst.find()) {
			return true;
		}
		return false;
	}

}
