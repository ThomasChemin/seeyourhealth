package util;

import java.util.regex.*;

public class PasswordVisitor extends AbstractVisitor {

	/**
	 * Filter for password
	 */
	@Override
	public boolean Visit(String password) {
		Pattern patternFirst = Pattern.compile("(?=^.{6,}$)(?=.*\\d)(?=.*[!?=#$]+)(?=.*[A-Z])(?=.*[a-z]).*(?!.*['{+&~.*<>%µ£¤:;,§()}@^_`|]).*");
		Matcher matcherFirst = patternFirst.matcher(password);
		if(matcherFirst.find()) {
			return true;
		}
		return false;
	}

}
