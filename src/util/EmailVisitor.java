package util;

import java.util.regex.*;

public class EmailVisitor extends AbstractVisitor {

	/**
	 * Filter email
	 */
	@Override
	public boolean Visit(String email) {
		Pattern patternFirst = Pattern.compile("[aA-zZ]+[@]{1}[a-z]+[.]{1}[a-z]{2,3}(?![-!?;$£^*µù%])");
		Matcher matcherFirst = patternFirst.matcher(email);
		if(matcherFirst.find()) {
			return true;
		}
		return false;
	}

}
