package util;

import java.util.regex.*;

public class CityVisitor extends AbstractVisitor {

	/**
	 * Filter city
	 */
	@Override
	public boolean Visit(String city) {
		Pattern patternFirst = Pattern.compile("^[A-Z][a-z]+(?![-!?,;.$£^*µù% ])\\b");
		Matcher matcherFirst = patternFirst.matcher(city);
		Pattern patternSecond = Pattern.compile("^[A-Z]{1}[a-z]+[-]{1}[A-Z]{1}[a-z]+(?![!?,;.$£^*µù%])\\b");
		Matcher matcherSecond = patternSecond.matcher(city);
		if(matcherFirst.find()) {
			return true;
		}
		if(matcherSecond.find()) {
			return true;
		}
		return false;
	}

}
