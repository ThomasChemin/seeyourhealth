package util;

import java.util.regex.*;

public class LastNameVisitor extends AbstractVisitor {

	/**
	 * Filter for lastName
	 */
	@Override
	public boolean Visit(String lastName) {
		Pattern patternFirst = Pattern.compile("^[A-Z][a-z]+(?![-!?,;.$£^*µù% ])\\b");
		Matcher matcherFirst = patternFirst.matcher(lastName);
		Pattern patternSecond = Pattern.compile("^[A-Z]{1}[a-z]+[-]{1}[A-Z]{1}[a-z]+(?![!?,;.$£^*µù%])\\b");
		Matcher matcherSecond = patternSecond.matcher(lastName);
		if(matcherFirst.find()) {
			return true;
		}
		if(matcherSecond.find()) {
			return true;
		}
		return false;
	}
}
