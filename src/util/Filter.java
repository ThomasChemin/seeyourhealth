package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Filter {

	private Map<String, Boolean> visitors = new HashMap<>();
	private Map<String, String> data = new HashMap<>();
	
	public Filter(Map<String, String> cData) {
		data = cData;
	}
	
	@Override
	public String toString() {
		return "Filter [visitors=" + visitors + "]";
	}

	/**
	 * Add in visitors the association name/bool
	 * @param key : the name of the attribute
	 * @param visitor : a Visitor
	 */
	public void acceptVisitor(String key, AbstractVisitor visitor) {
		visitors.put(key, visitor.Visit(data.get(key)));
	}
	
	/**
	 * Check if all visitors are true
	 * @return bool
	 */
	public boolean visit() {
		for (Map.Entry<String, Boolean> visitor : visitors.entrySet()) {
			if(visitor.getValue() == false) {
				return false;
			}
		}
		return true;
	}
}
