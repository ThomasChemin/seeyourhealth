package util;

import java.util.regex.*;

public class FirstNameVisitor extends AbstractVisitor {

	/**
	 * Filter firstName
	 */
	@Override
	public boolean Visit(String firstName) {
		Pattern patternFirst = Pattern.compile("^[A-Z][a-z]+(?![-!?,;.$£^*µù% ])\\b");
		Matcher matcherFirst = patternFirst.matcher(firstName);
		Pattern patternSecond = Pattern.compile("^[A-Z]{1}[a-z]+[-]{1}[A-Z]{1}[a-z]+(?![!?,;.$£^*µù%])\\b");
		Matcher matcherSecond = patternSecond.matcher(firstName);
		if(matcherFirst.find()) {
			return true;
		}
		if(matcherSecond.find()) {
			return true;
		}
		return false;
	}

}
