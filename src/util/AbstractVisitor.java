package util;

abstract public class AbstractVisitor {

	/**
	 * Check if the string passed is correct using regex
	 * @param data a string
	 * @return bool
	 */
	abstract public boolean Visit(String data);
}
