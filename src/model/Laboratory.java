package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Laboratory database table.
 * 
 */
@Entity
@Table(name = "Laboratory")
@NamedQuery(name="Laboratory.findAll", query="SELECT l FROM Laboratory l")
public class Laboratory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return id + ", adresse : " + adresse;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "adresse")
	private String adresse;

	@Column(name = "region")
	private String region;

	//bi-directional many-to-one association to Medecin
	@OneToMany(mappedBy="laboratory")
	private List<Medecin> medecins;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Laboratory() {
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public List<Medecin> getMedecins() {
		return this.medecins;
	}

	public void setMedecins(List<Medecin> medecins) {
		this.medecins = medecins;
	}

	public Medecin addMedecin(Medecin medecin) {
		getMedecins().add(medecin);
		medecin.setLaboratory(this);

		return medecin;
	}

	public Medecin removeMedecin(Medecin medecin) {
		getMedecins().remove(medecin);
		medecin.setLaboratory(null);

		return medecin;
	}

}