package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the Sample database table.
 * 
 */
@Entity
@Table(name = "Sample")
@NamedQuery(name="Sample.findAll", query="SELECT s FROM Sample s")
public class Sample implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return id + ", nom : " + name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	private String name;

	//bi-directional many-to-one association to Danger
	@ManyToOne
	@JoinColumn(name="danger", referencedColumnName="id")
	private Danger danger;

	//bi-directional many-to-one association to Test
	@OneToMany(mappedBy="sample")
	private List<Test> tests;

	public Sample() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Danger getDanger() {
		return this.danger;
	}

	public void setDanger(Danger danger) {
		this.danger = danger;
	}

	public List<Test> getTest() {
		return this.tests;
	}

	public void setTest(List<Test> tests) {
		this.tests = tests;
	}

}