package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the UserRight database table.
 * 
 */
@Entity
@Table(name = "UserRight")
@NamedQuery(name="UserRight.findAll", query="SELECT u FROM UserRight u")
public class UserRight implements Serializable {
	private static final long serialVersionUID = 1L;

	public void UserRightId(Users user, Rights right) {
        this.user = user;
        this.right = right;
    }
	
	//bi-directional many-to-one association to Rights
	@Id
	@ManyToOne
	@JoinColumn(name="idRight", referencedColumnName="id")
	private Rights right;

	//bi-directional many-to-one association to Users
	@Id
	@ManyToOne
	@JoinColumn(name="idUser", referencedColumnName="id")
	private Users user;

	public UserRight() {
	}

	public Rights getRight() {
		return this.right;
	}

	public void setRight(Rights right) {
		this.right = right;
	}

	public Users getUser() {
		return this.user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

}