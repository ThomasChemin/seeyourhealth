package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Danger database table.
 * 
 */
@Entity
@Table(name = "Danger")
@NamedQuery(name="Danger.findAll", query="SELECT d FROM Danger d")
public class Danger implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Niveau : " + lvl + ", nom : " + name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "lvl")
	private Integer lvl;

	@Column(name = "name")
	private String name;

	//bi-directional many-to-one association to Sample
	@OneToMany(mappedBy="danger")
	private List<Sample> samples;

	public Danger() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLvl() {
		return this.lvl;
	}

	public void setLvl(Integer lvl) {
		this.lvl = lvl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Sample> getSamples() {
		return this.samples;
	}

	public void setSamples(List<Sample> samples) {
		this.samples = samples;
	}

	public Sample addSample(Sample sample) {
		getSamples().add(sample);
		sample.setDanger(this);

		return sample;
	}

	public Sample removeSample(Sample sample) {
		getSamples().remove(sample);
		sample.setDanger(null);

		return sample;
	}

}