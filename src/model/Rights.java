package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Rights database table.
 * 
 */
@Entity
@Table(name = "Rights")
@NamedQuery(name="Rights.findAll", query="SELECT r FROM Rights r")
public class Rights implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	private String name;

	//bi-directional many-to-one association to UserRight
	@OneToMany(mappedBy="right")
	private List<UserRight> userRights;

	public Rights() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UserRight> getUserRights() {
		return this.userRights;
	}

	public void setUserRights(List<UserRight> userRights) {
		this.userRights = userRights;
	}

	public UserRight addUserRight(UserRight userRight) {
		getUserRights().add(userRight);
		userRight.setRight(this);

		return userRight;
	}

	public UserRight removeUserRight(UserRight userRight) {
		getUserRights().remove(userRight);
		userRight.setRight(null);

		return userRight;
	}

}