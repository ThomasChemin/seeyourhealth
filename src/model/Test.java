package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the Test database table.
 * 
 */
@Entity
@Table(name = "Test")
@NamedQuery(name="Test.findAll", query="SELECT t FROM Test t")
public class Test implements Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Test [result=" + result + "]";
	}

	public void UserRightId(Users user, Sample sample) {
        this.user = user;
        this.sample = sample;
    }
	
	@Column(name = "result")
	private String result;

	//bi-directional many-to-one association to Medecin
	@ManyToOne
	@JoinColumn(name="idMedRef", referencedColumnName="id")
	private Medecin medecin;

	//bi-directional many-to-one association to Users
	@Id
	@ManyToOne
	@JoinColumn(name="idUser", referencedColumnName="id")
	private Users user;

	//bi-directional many-to-one association to Sample
	@Id
	@ManyToOne
	@JoinColumn(name="idSample", referencedColumnName="id")
	private Sample sample;

	public Test() {
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Medecin getMedecin() {
		return this.medecin;
	}

	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}

	public Users getUser() {
		return this.user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Sample getSample() {
		return this.sample;
	}

	public void setSample(Sample sample) {
		this.sample = sample;
	}

}