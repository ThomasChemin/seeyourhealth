package tests;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import util.LastNameVisitor;

public class LastNameVisitorTest {
	String lastName;
	LastNameVisitor lastNameVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour LastNameVisitor");
	}
	
	@Before
	public void setUp() {
		lastName = "Chemin";
		lastNameVisitor = new LastNameVisitor();
	}
	
	/**
	 * Check if the lastName have his first letter in maj
	 */
	@Test
	public void checkPresenceOfMaj() {
		lastName = "chemin";
		assertFalse(lastNameVisitor.Visit(lastName));
	}
	
	/**
	 * Check if the lastName is correct when it doesn't have an hyphen
	 */
	@Test
	public void checkPresenceOfHyphenIfAbsent() {
		lastName = "Chemin Dutronc";
		assertFalse(lastNameVisitor.Visit(lastName));
	}
	
	/**
	 * Check if the lastName is correct when it does have an hyphen
	 */
	@Test
	public void checkPresenceOfHyphenIfPresent() {
		lastName = "Chemin-Dutronc";
		assertTrue(lastNameVisitor.Visit(lastName));
	}
	
	/**
	 * Check if the lastName is correct when it does have an hyphen but no maj on the second name
	 */
	@Test
	public void checkPresenceOfMajOnHyphen() {
		lastName = "Chemin-dutronc";
		assertFalse(lastNameVisitor.Visit(lastName));
	}
	
	/**
	 * Check if the firstName is correct when completed
	 */
	@Test
	public void checkLastNameCompleted() {
		lastName = "Chemin";
		assertTrue(lastNameVisitor.Visit(lastName));
		lastName = "Chemin-Dutronc";
		assertTrue(lastNameVisitor.Visit(lastName));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour LastNameVisitor");
	}
}
