package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.PhoneVisitor;

public class PhoneVisitorTest {
	String phone;
	PhoneVisitor phoneVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour phoneVisitor");
	}
	
	@Before
	public void setUp() {
		phone = "0474568495";
		phoneVisitor = new PhoneVisitor();
	}
	
	/**
	 * check if the phone is correct when it have spaces
	 */
	@Test
	public void checkAbenceOfSpaces() {
		phone = "04 74 56 84 95";
		assertFalse(phoneVisitor.Visit(phone));
	}
	
	/**
	 * check if the phone is correct when it have a +
	 */
	@Test
	public void checkAbsenceOfPlus() {
		phone = "+33474568495";
		assertFalse(phoneVisitor.Visit(phone));
	}
	
	/**
	 * check if the phone is correct if it contain string
	 */
	@Test
	public void checkAbsenceOfString() {
		phone = "36448aeza";
		assertFalse(phoneVisitor.Visit(phone));
	}
	
	/**
	 * check if the phone is correct if it have 10 digits
	 */
	@Test
	public void checkPresenceOfTenNumber() {
		phone = "04745684954";
		assertFalse(phoneVisitor.Visit(phone));
		phone = "047456849";
		assertFalse(phoneVisitor.Visit(phone));
		phone = "0474568494";
		assertTrue(phoneVisitor.Visit(phone));
	}
	
	/**
	 * check if the phone is correct if it have a 0 at the beginning
	 */
	@Test
	public void checkPresenceOfZeroAtTheBeginning() {
		phone = "0474568494";
		assertTrue(phoneVisitor.Visit(phone));
		phone = "7474568497";
		assertFalse(phoneVisitor.Visit(phone));
	}
	
	/**
	 * check if the phone is correct when it have a 0 at the beginning without spaces and +
	 */
	@Test
	public void checkPhoneCompleted() {
		phone = "0474568495";
		assertTrue(phoneVisitor.Visit(phone));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour PhoneVisitor");
	}
}
