package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.CityVisitor;

public class CityVisitorTest {
	String city;
	CityVisitor cityVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour CityVisitor");
	}
	
	@Before
	public void setUp() {
		city = "Lyon";
		cityVisitor = new CityVisitor();
	}
	
	/**
	 * Check if the first letter is a maj
	 */
	@Test
	public void checkPresenceOfMaj() {
		city = "lyon";
		assertFalse(cityVisitor.Visit(city));
	}
	
	/**
	 * Check if the city doesn't contain a number
	 */
	@Test
	public void checkAbsenceOfNumber() {
		city = "Lyon 456";
		assertFalse(cityVisitor.Visit(city));
	}
	
	/**
	 * Check if the city is correct when it doesn't contain a number and have a maj
	 */
	@Test
	public void checkCityCompleted() {
		city = "Lyon";
		assertTrue(cityVisitor.Visit(city));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour CityVisitor");
	}
}
