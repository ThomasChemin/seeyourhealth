package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.PasswordVisitor;

public class PasswordVisitorTest {
	String password;
	PasswordVisitor passwordVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour PasswordVisitor");
	}
	
	@Before
	public void setUp() {
		password = "Test$69";
		passwordVisitor = new PasswordVisitor();
	}
	
	/**
	 * Check if the password is correct when it doesn't have a maj
	 */
	@Test
	public void checkAbsenceOfMaj() {
		password = "test$69";
		assertFalse(passwordVisitor.Visit(password));
	}
	
	/**
	 * Check if the password is correct if it doesn't have a special char
	 */
	@Test
	public void checkAbsenceOfSpecialChar() {
		password = "Test69";
		assertFalse(passwordVisitor.Visit(password));
	}
	
	/**
	 * Check if the password is correct when it doesn't have digit
	 */
	@Test
	public void checkAbsenceOfNumber() {
		password = "test$";
		assertFalse(passwordVisitor.Visit(password));
	}
	
	/**
	 * Check if the password is correct when it doesn't have at least 6 char
	 */
	@Test
	public void checkPresenceOfAtLeastSixChar() {
		password = "Tes$6";
		assertFalse(passwordVisitor.Visit(password));
	}
	
	/**
	 * Check if the password is correct when it contains a maj, letter, special char and digit with at least 6 char
	 */
	@Test
	public void checkPasswordCompleted() {
		password = "Test$69";
		assertTrue(passwordVisitor.Visit(password));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour PasswordVisitor");
	}
}
