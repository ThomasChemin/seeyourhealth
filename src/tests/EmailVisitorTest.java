package tests;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import util.EmailVisitor;

public class EmailVisitorTest {
	String email;
	EmailVisitor emailVisitor;
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour EmailVisitor");
	}
	
	@Before
	public void setUp() {
		email = "test@gmail.com";
		emailVisitor = new EmailVisitor();
	}
	
	/**
	 * check if the email contain @
	 */
	@Test
	public void checkPresenceSeparator() {
		email = "testgmail.com";
		assertFalse(emailVisitor.Visit(email));
	}
	
	/**
	 * check if the email contain .
	 */
	@Test
	public void checkPresenceOfDot() {
		email = "test@gmailcom";
		assertFalse(emailVisitor.Visit(email));
	}
	
	/**
	 * check if the email contain the extension (fr, com...)
	 */
	@Test
	public void checkPresenceOfExtension() {
		email = "test@gmail";
		assertFalse(emailVisitor.Visit(email));
	}
	
	/**
	 * Check if the email contain a body 
	 */
	@Test
	public void checkPresenceOfLocal() {
		email = "@gmail.com";
		assertFalse(emailVisitor.Visit(email));
	}
	
	/**
	 * check if the email is correct when completed
	 */
	@Test
	public void checkEmailCompleted() {
		email = "test@gmail.com";
		assertTrue(emailVisitor.Visit(email));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour EmailVisitor");
	}
}
