package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.AdresseVisitor;

public class AdresseVisitorTest {
	String adresse;
	AdresseVisitor adresseVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour AdresseVisitor");
	}
	
	@Before
	public void setUp() {
		adresse = "456 rue du pont";
		adresseVisitor = new AdresseVisitor();
	}
	
	/**
	 * Check if the adresse contain numbers
	 */
	@Test
	public void checkPresenceOfNumber() {
		adresse = "Rue du pont";
		assertFalse(adresseVisitor.Visit(adresse));
	}
	
	/**
	 * Check if the adresse contain a string
	 */
	@Test
	public void checkPresenceOfString() {
		adresse = "456";
		assertFalse(adresseVisitor.Visit(adresse));
	}
	
	/**
	 * check if the adresse is correct when there is a number and a string
	 */
	@Test
	public void checkAdresseCompleted() {
		adresse = "456 rue du pont";
		assertTrue(adresseVisitor.Visit(adresse));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour AdresseVisitor");
	}
}
