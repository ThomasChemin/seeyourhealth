package tests;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import util.FirstNameVisitor;

public class FirstNameVisitorTest {
	String firstName;
	FirstNameVisitor firstNameVisitor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("Demarrage du test pour firstNameVisitor");
	}
	
	@Before
	public void setUp() {
		firstName = "Thomas";
		firstNameVisitor = new FirstNameVisitor();
	}
	
	/**
	 * Check if the firstName have his first letter in maj
	 */
	@Test
	public void checkPresenceOfMaj() {
		firstName = "thomas";
		assertFalse(firstNameVisitor.Visit(firstName));
	}
	
	/**
	 * Check if the firstName is correct when it doesn't have an hyphen
	 */
	@Test
	public void checkPresenceOfHyphenIfAbsent() {
		firstName = "Thomas Eude";
		assertFalse(firstNameVisitor.Visit(firstName));
	}
	
	/**
	 * Check if the firstName is correct when it does have an hyphen
	 */
	@Test
	public void checkPresenceOfHyphenIfPresent() {
		firstName = "Thomas-Eude";
		assertTrue(firstNameVisitor.Visit(firstName));
	}
	
	/**
	 * Check if the firstName is correct when it does have an hyphen but no maj on se second name
	 */
	@Test
	public void checkPresenceOfMajOnHyphen() {
		firstName = "Thomas-eude";
		assertFalse(firstNameVisitor.Visit(firstName));
	}
	
	/**
	 * check the firstName when it is completed
	 */
	@Test
	public void checkfirstNameCompleted() {
		firstName = "Thomas";
		assertTrue(firstNameVisitor.Visit(firstName));
		firstName = "Thomas-Eude";
		assertTrue(firstNameVisitor.Visit(firstName));
	}
	
	@After
	public void tearDown() {
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("Fin du test pour FirstNameVisitor");
	}
}
