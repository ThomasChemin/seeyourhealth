import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import Controller.ControllerConnexion;
import DAO.DAOMedecin;
import DAO.DAORights;
import DAO.DAOUser;
import DAO.DAOUserRight;
import model.Laboratory;
import model.Medecin;
import model.Rights;
import model.Sample;
import model.Test;
import model.Users;
import util.HibernateUtil;
import views.Connexion;

/**
 * @author student
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ControllerConnexion(new Connexion(), 
				new DAOUser(HibernateUtil.getSessionFactory().openSession()),
				new DAORights(HibernateUtil.getSessionFactory().openSession()),
				new DAOUserRight(HibernateUtil.getSessionFactory().openSession())
				).init();
	}
}