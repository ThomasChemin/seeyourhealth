package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Color;
import javax.swing.JComboBox;

public class Connexion extends JFrame {

	private JPanel contentPane;
	private JButton btnConnexion;
	private JButton btnInscription;
	private JPanel panelBase;
	private JPanel panelConnexion;
	private JPanel panelInscription;
	private JPanel panelAccueil;
	private JButton btnInscriptionConnexion;
	private JButton btnConnexionInscription;
	private JPanel panel;
	private JTextField txtConnexionMail;
	private JLabel lblConnexionEmail;
	private JLabel lblConnexionPassword;
	private JPasswordField pswConnexionPassword;
	private JButton btnConnexionConnexion;
	private JLabel lblConnexionErrors;
	private JButton btnInscriptionInscription;
	private JPanel panel_1;
	private JTextField txtInscriptionLastName;
	private JLabel lblInscriptionLastName;
	private JTextField txtIncriptionFirstName;
	private JLabel lblFirstName;
	private JTextField txtInscriptionAddresse;
	private JLabel lblInscriptionAddresse;
	private JTextField txtInscriptionCity;
	private JLabel lblInscriptionVille;
	private JTextField txtInscriptionPhone;
	private JLabel lblInscriptionTelephone;
	private JTextField txtInscriptionEmail;
	private JLabel lblIncriptionEmail;
	private JPasswordField pswInscriptionPassword;
	private JLabel lblInscriptionPassword;
	private JPasswordField pswInscriptionPasswordConfirmation;
	private JLabel lblInscriptionPasswordConfirm;
	private JPanel panel_2;
	private JLabel lblInscriptionerrors;
	private JLabel lblInscriptionSuccess;
	private JPanel panel_3;
	private JPanel panel_4;
	private JLabel lblConnexionSuccess;
	private JComboBox cbConnexionChooseRight;
	private JButton btnConnexionChooseRight;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Connexion frame = new Connexion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Connexion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 729, 502);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		panelBase = new JPanel();
		contentPane.add(panelBase, BorderLayout.CENTER);
		panelBase.setLayout(new CardLayout(0, 0));
		
		panelAccueil = new JPanel();
		panelBase.add(panelAccueil, "name_3100644687964");
		
		panel_2 = new JPanel();
		panelAccueil.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblWelcome = new JLabel("Bienvenue sur SeeYourHealth");
		panel_2.add(lblWelcome);
		lblWelcome.setName("Welcome");
		
		JLabel lblConnexion = new JLabel("Se Connecter");
		panel_2.add(lblConnexion);
		lblConnexion.setName("lblConnexion");
		
		btnConnexion = new JButton("Connexion");
		panel_2.add(btnConnexion);
		btnConnexion.setName("Connexion");
		
		JLabel lblInscription = new JLabel("S'inscrire");
		panel_2.add(lblInscription);
		lblInscription.setName("lblInscription");
		
		btnInscription = new JButton("Inscription");
		panel_2.add(btnInscription);
		btnInscription.setName("Inscription");
		
		panelConnexion = new JPanel();
		panelBase.add(panelConnexion, "name_3236988150864");
		GridBagLayout gbl_panelConnexion = new GridBagLayout();
		gbl_panelConnexion.columnWidths = new int[]{50, 50, 500, 100, 0};
		gbl_panelConnexion.rowHeights = new int[]{57, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelConnexion.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelConnexion.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelConnexion.setLayout(gbl_panelConnexion);
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.gridx = 2;
		gbc_panel.gridy = 0;
		panelConnexion.add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		lblConnexionErrors = new JLabel("");
		lblConnexionErrors.setForeground(Color.RED);
		lblConnexionErrors.setName("lblConnexionErrors");
		panel.add(lblConnexionErrors);
		
		lblConnexionSuccess = new JLabel("");
		lblConnexionSuccess.setForeground(Color.GREEN);
		lblConnexionSuccess.setName("lblConnexionSuccess");
		panel.add(lblConnexionSuccess);
		
		lblConnexionEmail = new JLabel("Email");
		lblConnexionEmail.setName("lblConnexionEmail");
		panel.add(lblConnexionEmail);
		
		txtConnexionMail = new JTextField();
		txtConnexionMail.setName("txtConnexionMail");
		panel.add(txtConnexionMail);
		txtConnexionMail.setColumns(10);
		
		lblConnexionPassword = new JLabel("Mot de passe");
		lblConnexionPassword.setName("lblConnexionPassword");
		panel.add(lblConnexionPassword);
		
		pswConnexionPassword = new JPasswordField();
		pswConnexionPassword.setName("pswConnexionPassword");
		panel.add(pswConnexionPassword);
		
		cbConnexionChooseRight = new JComboBox();
		cbConnexionChooseRight.setVisible(false);
		cbConnexionChooseRight.setName("cbConnexionChooseRight");
		GridBagConstraints gbc_cbConnexionChooseRight = new GridBagConstraints();
		gbc_cbConnexionChooseRight.insets = new Insets(0, 0, 5, 5);
		gbc_cbConnexionChooseRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbConnexionChooseRight.gridx = 2;
		gbc_cbConnexionChooseRight.gridy = 3;
		panelConnexion.add(cbConnexionChooseRight, gbc_cbConnexionChooseRight);
		
		btnConnexionChooseRight = new JButton("Choisir ce rôle");
		btnConnexionChooseRight.setVisible(false);
		btnConnexionChooseRight.setName("btnConnexionChooseRight");
		GridBagConstraints gbc_btnConnexionChooseRight = new GridBagConstraints();
		gbc_btnConnexionChooseRight.insets = new Insets(0, 0, 5, 5);
		gbc_btnConnexionChooseRight.gridx = 2;
		gbc_btnConnexionChooseRight.gridy = 4;
		panelConnexion.add(btnConnexionChooseRight, gbc_btnConnexionChooseRight);
		
		panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.gridx = 2;
		gbc_panel_4.gridy = 8;
		panelConnexion.add(panel_4, gbc_panel_4);
		
		btnConnexionInscription = new JButton("Je n'ai pas de compte");
		panel_4.add(btnConnexionInscription);
		btnConnexionInscription.setName("btnConnexionInscription");
		
		btnConnexionConnexion = new JButton("Connexion");
		panel_4.add(btnConnexionConnexion);
		btnConnexionConnexion.setName("btnConnexionConnexion");
		
		panelInscription = new JPanel();
		panelBase.add(panelInscription, "name_3253527137084");
		
		panel_1 = new JPanel();
		panelInscription.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		lblInscriptionSuccess = new JLabel("");
		lblInscriptionSuccess.setForeground(Color.GREEN);
		lblInscriptionSuccess.setName("lblInscriptionSuccess");
		panel_1.add(lblInscriptionSuccess);
		
		lblInscriptionerrors = new JLabel("");
		lblInscriptionerrors.setForeground(Color.RED);
		lblInscriptionerrors.setName("lblInscriptionerrors");
		panel_1.add(lblInscriptionerrors);
		
		lblInscriptionLastName = new JLabel("Nom");
		lblInscriptionLastName.setName("lblInscriptionLastName");
		panel_1.add(lblInscriptionLastName);
		
		txtInscriptionLastName = new JTextField();
		txtInscriptionLastName.setName("txtInscriptionLastName");
		panel_1.add(txtInscriptionLastName);
		txtInscriptionLastName.setColumns(10);
		
		lblFirstName = new JLabel("Prenom");
		lblFirstName.setName("lblFirstName");
		panel_1.add(lblFirstName);
		
		txtIncriptionFirstName = new JTextField();
		txtIncriptionFirstName.setName("txtIncriptionFirstName");
		panel_1.add(txtIncriptionFirstName);
		txtIncriptionFirstName.setColumns(10);
		
		lblInscriptionAddresse = new JLabel("Addresse");
		lblInscriptionAddresse.setName("lblInscriptionAddresse");
		panel_1.add(lblInscriptionAddresse);
		
		txtInscriptionAddresse = new JTextField();
		txtInscriptionAddresse.setName("txtInscriptionAddresse");
		panel_1.add(txtInscriptionAddresse);
		txtInscriptionAddresse.setColumns(10);
		
		lblInscriptionVille = new JLabel("Ville");
		lblInscriptionVille.setName("lblInscriptionVille");
		panel_1.add(lblInscriptionVille);
		
		txtInscriptionCity = new JTextField();
		txtInscriptionCity.setName("txtInscriptionCity");
		panel_1.add(txtInscriptionCity);
		txtInscriptionCity.setColumns(10);
		
		lblInscriptionTelephone = new JLabel("Téléphone");
		lblInscriptionTelephone.setName("lblInscriptionTelephone");
		panel_1.add(lblInscriptionTelephone);
		
		txtInscriptionPhone = new JTextField();
		txtInscriptionPhone.setName("txtInscriptionPhone");
		panel_1.add(txtInscriptionPhone);
		txtInscriptionPhone.setColumns(10);
		
		lblIncriptionEmail = new JLabel("Email");
		lblIncriptionEmail.setName("lblInscriptionEmail");
		panel_1.add(lblIncriptionEmail);
		
		txtInscriptionEmail = new JTextField();
		txtInscriptionEmail.setName("txtInscriptionEmail");
		panel_1.add(txtInscriptionEmail);
		txtInscriptionEmail.setColumns(10);
		
		lblInscriptionPassword = new JLabel("Mot de passe");
		lblInscriptionPassword.setName("lblInscriptionPassword");
		panel_1.add(lblInscriptionPassword);
		
		pswInscriptionPassword = new JPasswordField();
		pswInscriptionPassword.setName("pswInscriptionPassword");
		panel_1.add(pswInscriptionPassword);
		
		lblInscriptionPasswordConfirm = new JLabel("Confirmation du mot de passe");
		lblInscriptionPasswordConfirm.setName("lblInscriptionPasswordConfirm");
		panel_1.add(lblInscriptionPasswordConfirm);
		
		pswInscriptionPasswordConfirmation = new JPasswordField();
		pswInscriptionPasswordConfirmation.setName("pswInscriptionPasswordConfirmation");
		panel_1.add(pswInscriptionPasswordConfirmation);
		
		panel_3 = new JPanel();
		panelInscription.add(panel_3);
		
		btnInscriptionConnexion = new JButton("J'ai déjà un compte");
		panel_3.add(btnInscriptionConnexion);
		btnInscriptionConnexion.setName("btnInscriptionConnexion");
		
		btnInscriptionInscription = new JButton("S'inscrire");
		panel_3.add(btnInscriptionInscription);
		btnInscriptionInscription.setName("btnInscriptionInscription");
		btnInscriptionConnexion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
	}
	
	public JButton getBtnConnexion() {
		return btnConnexion;
	}
	public JButton getBtnInscription() {
		return btnInscription;
	}
	public JPanel getPanelBase() {
		return panelBase;
	}
	public JPanel getPanelConnexion() {
		return panelConnexion;
	}
	public JPanel getPanelInscription() {
		return panelInscription;
	}
	public JPanel getPanelAccueil() {
		return panelAccueil;
	}
	public JButton getBtnInscriptionConnexion() {
		return btnInscriptionConnexion;
	}
	public JButton getBtnConnexionInscription() {
		return btnConnexionInscription;
	}
	public JTextField getTxtConnexionMail() {
		return txtConnexionMail;
	}
	public JButton getBtnConnexionConnexion() {
		return btnConnexionConnexion;
	}
	public JPasswordField getPswConnexionPassword() {
		return pswConnexionPassword;
	}
	public JLabel getLblConnexionErrors() {
		return lblConnexionErrors;
	}
	public JButton getBtnInscriptionInscription() {
		return btnInscriptionInscription;
	}
	public JTextField getTxtIncriptionFirstName() {
		return txtIncriptionFirstName;
	}
	public JTextField getTxtInscriptionAddresse() {
		return txtInscriptionAddresse;
	}
	public JTextField getTxtInscriptionCity() {
		return txtInscriptionCity;
	}
	public JTextField getTxtInscriptionPhone() {
		return txtInscriptionPhone;
	}
	public JTextField getTxtInscriptionEmail() {
		return txtInscriptionEmail;
	}
	public JPasswordField getPswInscriptionPassword() {
		return pswInscriptionPassword;
	}
	public JPasswordField getPswInscriptionPasswordConfirmation() {
		return pswInscriptionPasswordConfirmation;
	}
	public JLabel getLblInscriptionerrors() {
		return lblInscriptionerrors;
	}
	public JTextField getTxtInscriptionLastName() {
		return txtInscriptionLastName;
	}
	public JLabel getLblConnexionSuccess() {
		return lblConnexionSuccess;
	}
	public JLabel getLblInscriptionSuccess() {
		return lblInscriptionSuccess;
	}
	public JComboBox getCbConnexionChooseRight() {
		return cbConnexionChooseRight;
	}
	public JButton getBtnConnexionChooseRight() {
		return btnConnexionChooseRight;
	}
}
