package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import DAO.DAOSample;
import model.Sample;
import util.HibernateUtil;

import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;

import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JTextField;

public class Admin extends JFrame {

	private JPanel contentPane;
	private JPanel panelCardAdmin;
	private JLabel lblConnexionStatus;
	private JPanel panelAdminChangeUserRights;
	private JTable jTableUserRights;
	private JPanel panelAdminChangeUserRightsCB;
	private JComboBox cbUsers;
	private JLabel lblUserLastName;
	private JLabel lblUserFirstName;
	private JLabel lblUserEmail;
	private JScrollPane scrollPaneUsers;
	private JButton btnAdminSupprimer;
	private JButton btnAdminAjouter;
	private JComboBox cbAdminRights;
	private JPanel panel;
	private JButton btnModifierMedicament;
	private JButton btnAjouterMedicament;
	private JButton btnModifierRoles;
	private JPanel panelAdminModifMedoc;
	private JPanel panelAdminAjouterMedoc;
	private JLabel lblMedocAjouterName;
	private JLabel lblMedocAjouterDanger;
	private JTextField txtMedocAjouterName;
	private JComboBox cbMedocAjouterDanger;
	private JLabel lblAjouterMedicamentTitle;
	private JButton btnMedocAjouterAjouter;
	private JLabel lblModifMedicamentMedicamentTitle;
	private JLabel lblModifMedicamentDangerTitle;
	private JComboBox cbModifMedicamentMedicament;
	private JComboBox cbModifMedicamentDanger;
	private JButton btnModifMedicamentModifier;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 516);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panelCardAdmin = new JPanel();
		panelCardAdmin.setName("panelCardAdmin");
		contentPane.add(panelCardAdmin, BorderLayout.NORTH);
		panelCardAdmin.setLayout(new CardLayout(0, 0));
		
		panelAdminChangeUserRights = new JPanel();
		panelAdminChangeUserRights.setName("panelAdminChangeUserRights");
		panelCardAdmin.add(panelAdminChangeUserRights, "name_555487124472");
		panelAdminChangeUserRights.setLayout(new BoxLayout(panelAdminChangeUserRights, BoxLayout.X_AXIS));
		
		panelAdminChangeUserRightsCB = new JPanel();
		panelAdminChangeUserRights.add(panelAdminChangeUserRightsCB);
		panelAdminChangeUserRightsCB.setName("panelAdminChangeUserRightsCB");
		GridBagLayout gbl_panelAdminChangeUserRightsCB = new GridBagLayout();
		gbl_panelAdminChangeUserRightsCB.columnWidths = new int[]{333, 0};
		gbl_panelAdminChangeUserRightsCB.rowHeights = new int[]{24, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 0, 0, 0, 0};
		gbl_panelAdminChangeUserRightsCB.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelAdminChangeUserRightsCB.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelAdminChangeUserRightsCB.setLayout(gbl_panelAdminChangeUserRightsCB);
		
		cbUsers = new JComboBox();
		cbUsers.setName("cbUsers");
		GridBagConstraints gbc_cbUsers = new GridBagConstraints();
		gbc_cbUsers.insets = new Insets(0, 0, 5, 0);
		gbc_cbUsers.gridx = 0;
		gbc_cbUsers.gridy = 0;
		panelAdminChangeUserRightsCB.add(cbUsers, gbc_cbUsers);
		
		lblUserLastName = new JLabel("Nom : ");
		lblUserLastName.setName("lblUserLastName");
		GridBagConstraints gbc_lblUserLastName = new GridBagConstraints();
		gbc_lblUserLastName.anchor = GridBagConstraints.WEST;
		gbc_lblUserLastName.insets = new Insets(0, 0, 5, 0);
		gbc_lblUserLastName.gridx = 0;
		gbc_lblUserLastName.gridy = 1;
		panelAdminChangeUserRightsCB.add(lblUserLastName, gbc_lblUserLastName);
		
		lblUserFirstName = new JLabel("Prénom : ");
		lblUserFirstName.setName("lblUserFirstName");
		GridBagConstraints gbc_lblUserFirstName = new GridBagConstraints();
		gbc_lblUserFirstName.anchor = GridBagConstraints.WEST;
		gbc_lblUserFirstName.insets = new Insets(0, 0, 5, 0);
		gbc_lblUserFirstName.gridx = 0;
		gbc_lblUserFirstName.gridy = 2;
		panelAdminChangeUserRightsCB.add(lblUserFirstName, gbc_lblUserFirstName);
		
		lblUserEmail = new JLabel("Email : ");
		lblUserEmail.setName("lblUserEmail");
		GridBagConstraints gbc_lblUserEmail = new GridBagConstraints();
		gbc_lblUserEmail.insets = new Insets(0, 0, 5, 0);
		gbc_lblUserEmail.anchor = GridBagConstraints.WEST;
		gbc_lblUserEmail.gridx = 0;
		gbc_lblUserEmail.gridy = 3;
		panelAdminChangeUserRightsCB.add(lblUserEmail, gbc_lblUserEmail);		
		
		btnAdminAjouter = new JButton("Ajouter");
		btnAdminAjouter.setName("btnAdminAjouter");
		GridBagConstraints gbc_btnAdminAjouter = new GridBagConstraints();
		gbc_btnAdminAjouter.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdminAjouter.gridx = 0;
		gbc_btnAdminAjouter.gridy = 8;
		panelAdminChangeUserRightsCB.add(btnAdminAjouter, gbc_btnAdminAjouter);
		
		cbAdminRights = new JComboBox();
		cbAdminRights.setName("cbAdminRights");
		GridBagConstraints gbc_cbAdminRights = new GridBagConstraints();
		gbc_cbAdminRights.insets = new Insets(0, 0, 5, 0);
		gbc_cbAdminRights.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbAdminRights.gridx = 0;
		gbc_cbAdminRights.gridy = 9;
		panelAdminChangeUserRightsCB.add(cbAdminRights, gbc_cbAdminRights);
		
		btnAdminSupprimer = new JButton("Supprimer");
		btnAdminSupprimer.setName("btnAdminSupprimer");
		GridBagConstraints gbc_btnAdminSupprimer = new GridBagConstraints();
		gbc_btnAdminSupprimer.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdminSupprimer.gridx = 0;
		gbc_btnAdminSupprimer.gridy = 12;
		panelAdminChangeUserRightsCB.add(btnAdminSupprimer, gbc_btnAdminSupprimer);
		
		jTableUserRights = new JTable();
		jTableUserRights.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"New column", "New column", "New column"
			}
		));
		jTableUserRights.setName("jTableUserRights");
		
		scrollPaneUsers = new JScrollPane(jTableUserRights);
		scrollPaneUsers.setName("scrollPane");
		panelAdminChangeUserRights.add(scrollPaneUsers);
		
		panelAdminModifMedoc = new JPanel();
		panelAdminModifMedoc.setName("panelAdminModifMedoc");
		panelCardAdmin.add(panelAdminModifMedoc, "name_954109751932");
		GridBagLayout gbl_panelAdminModifMedoc = new GridBagLayout();
		gbl_panelAdminModifMedoc.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0};
		gbl_panelAdminModifMedoc.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelAdminModifMedoc.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelAdminModifMedoc.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelAdminModifMedoc.setLayout(gbl_panelAdminModifMedoc);
		
		lblModifMedicamentMedicamentTitle = new JLabel("Médicament");
		lblModifMedicamentMedicamentTitle.setName("lblModifMedicamentMedicamentTitle");
		GridBagConstraints gbc_lblModifMedicamentMedicamentTitle = new GridBagConstraints();
		gbc_lblModifMedicamentMedicamentTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblModifMedicamentMedicamentTitle.gridx = 7;
		gbc_lblModifMedicamentMedicamentTitle.gridy = 4;
		panelAdminModifMedoc.add(lblModifMedicamentMedicamentTitle, gbc_lblModifMedicamentMedicamentTitle);
		
		lblModifMedicamentDangerTitle = new JLabel("Danger");
		lblModifMedicamentDangerTitle.setName("lblModifMedicamentDangerTitle");
		GridBagConstraints gbc_lblModifMedicamentDangerTitle = new GridBagConstraints();
		gbc_lblModifMedicamentDangerTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblModifMedicamentDangerTitle.gridx = 17;
		gbc_lblModifMedicamentDangerTitle.gridy = 4;
		panelAdminModifMedoc.add(lblModifMedicamentDangerTitle, gbc_lblModifMedicamentDangerTitle);
		
		cbModifMedicamentMedicament = new JComboBox();
		cbModifMedicamentMedicament.setName("cbModifMedicamentMedicament");
		GridBagConstraints gbc_cbModifMedicamentMedicament = new GridBagConstraints();
		gbc_cbModifMedicamentMedicament.insets = new Insets(0, 0, 5, 5);
		gbc_cbModifMedicamentMedicament.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbModifMedicamentMedicament.gridx = 7;
		gbc_cbModifMedicamentMedicament.gridy = 6;
		panelAdminModifMedoc.add(cbModifMedicamentMedicament, gbc_cbModifMedicamentMedicament);
		
		cbModifMedicamentDanger = new JComboBox();
		cbModifMedicamentDanger.setName("cbModifMedicamentDanger");
		GridBagConstraints gbc_cbModifMedicamentDanger = new GridBagConstraints();
		gbc_cbModifMedicamentDanger.insets = new Insets(0, 0, 5, 0);
		gbc_cbModifMedicamentDanger.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbModifMedicamentDanger.gridx = 17;
		gbc_cbModifMedicamentDanger.gridy = 6;
		panelAdminModifMedoc.add(cbModifMedicamentDanger, gbc_cbModifMedicamentDanger);
		
		btnModifMedicamentModifier = new JButton("Modifier");
		btnModifMedicamentModifier.setName("btnModifMedicamentModifier");
		GridBagConstraints gbc_btnModifMedicamentModifier = new GridBagConstraints();
		gbc_btnModifMedicamentModifier.insets = new Insets(0, 0, 0, 5);
		gbc_btnModifMedicamentModifier.gridx = 12;
		gbc_btnModifMedicamentModifier.gridy = 13;
		panelAdminModifMedoc.add(btnModifMedicamentModifier, gbc_btnModifMedicamentModifier);
		
		panelAdminAjouterMedoc = new JPanel();
		panelAdminAjouterMedoc.setName("panelAdminAjouterMedoc");
		panelCardAdmin.add(panelAdminAjouterMedoc, "name_1011633374204");
		GridBagLayout gbl_panelAdminAjouterMedoc = new GridBagLayout();
		gbl_panelAdminAjouterMedoc.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 250, 0};
		gbl_panelAdminAjouterMedoc.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelAdminAjouterMedoc.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelAdminAjouterMedoc.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelAdminAjouterMedoc.setLayout(gbl_panelAdminAjouterMedoc);
		
		lblAjouterMedicamentTitle = new JLabel("Ajouter un médicament");
		lblAjouterMedicamentTitle.setName("lblAjouterMedicamentTitle");
		GridBagConstraints gbc_lblAjouterMedicamentTitle = new GridBagConstraints();
		gbc_lblAjouterMedicamentTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblAjouterMedicamentTitle.gridx = 10;
		gbc_lblAjouterMedicamentTitle.gridy = 0;
		panelAdminAjouterMedoc.add(lblAjouterMedicamentTitle, gbc_lblAjouterMedicamentTitle);
		
		lblMedocAjouterName = new JLabel("Nom : ");
		lblMedocAjouterName.setName("lblMedocAjouterName");
		GridBagConstraints gbc_lblMedocAjouterName = new GridBagConstraints();
		gbc_lblMedocAjouterName.insets = new Insets(0, 0, 5, 5);
		gbc_lblMedocAjouterName.gridx = 8;
		gbc_lblMedocAjouterName.gridy = 3;
		panelAdminAjouterMedoc.add(lblMedocAjouterName, gbc_lblMedocAjouterName);
		
		txtMedocAjouterName = new JTextField();
		txtMedocAjouterName.setName("txtMedocAjouterName");
		GridBagConstraints gbc_txtMedocAjouterName = new GridBagConstraints();
		gbc_txtMedocAjouterName.insets = new Insets(0, 0, 5, 0);
		gbc_txtMedocAjouterName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMedocAjouterName.gridx = 10;
		gbc_txtMedocAjouterName.gridy = 3;
		panelAdminAjouterMedoc.add(txtMedocAjouterName, gbc_txtMedocAjouterName);
		txtMedocAjouterName.setColumns(10);
		
		lblMedocAjouterDanger = new JLabel("Danger : ");
		lblMedocAjouterDanger.setName("lblMedocAjouterDanger");
		GridBagConstraints gbc_lblMedocAjouterDanger = new GridBagConstraints();
		gbc_lblMedocAjouterDanger.insets = new Insets(0, 0, 5, 5);
		gbc_lblMedocAjouterDanger.gridx = 8;
		gbc_lblMedocAjouterDanger.gridy = 4;
		panelAdminAjouterMedoc.add(lblMedocAjouterDanger, gbc_lblMedocAjouterDanger);
		
		cbMedocAjouterDanger = new JComboBox();
		cbMedocAjouterDanger.setName("cbMedocAjouterDanger");
		GridBagConstraints gbc_cbMedocAjouterDanger = new GridBagConstraints();
		gbc_cbMedocAjouterDanger.insets = new Insets(0, 0, 5, 0);
		gbc_cbMedocAjouterDanger.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbMedocAjouterDanger.gridx = 10;
		gbc_cbMedocAjouterDanger.gridy = 4;
		panelAdminAjouterMedoc.add(cbMedocAjouterDanger, gbc_cbMedocAjouterDanger);
		
		btnMedocAjouterAjouter = new JButton("Ajouter");
		btnMedocAjouterAjouter.setName("btnMedocAjouterAjouter");
		GridBagConstraints gbc_btnMedocAjouterAjouter = new GridBagConstraints();
		gbc_btnMedocAjouterAjouter.gridx = 10;
		gbc_btnMedocAjouterAjouter.gridy = 8;
		panelAdminAjouterMedoc.add(btnMedocAjouterAjouter, gbc_btnMedocAjouterAjouter);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		lblConnexionStatus = new JLabel("Connecté en tant que : ");
		panel.add(lblConnexionStatus);
		lblConnexionStatus.setName("lblConnexionStatus");
		
		btnModifierMedicament = new JButton("Modifier un médicament");
		btnModifierMedicament.setName("btnModifierMedicament");
		panel.add(btnModifierMedicament);
		
		btnAjouterMedicament = new JButton("Ajouter un médicament");
		btnAjouterMedicament.setName("btnAjouterMedicament");
		panel.add(btnAjouterMedicament);
		
		btnModifierRoles = new JButton("Modifier les rôles d'un utilisateur");
		btnModifierRoles.setName("btnModifierRoles");
		panel.add(btnModifierRoles);
	}

	public JPanel getPanelCardAdmin() {
		return panelCardAdmin;
	}
	public JLabel getLblConnexionStatus() {
		return lblConnexionStatus;
	}
	public JPanel getPanelAdminChangeUserRights() {
		return panelAdminChangeUserRights;
	}
	public JPanel getPanelAdminChangeUserRightsCB() {
		return panelAdminChangeUserRightsCB;
	}
	public JLabel getLblUserFirstName() {
		return lblUserFirstName;
	}
	public JComboBox getCbUsers() {
		return cbUsers;
	}
	public JLabel getLblUserEmail() {
		return lblUserEmail;
	}
	public JLabel getLblUserLastName() {
		return lblUserLastName;
	}
	public JTable getJTableUserRights() {
		return jTableUserRights;
	}
	public JButton getBtnAdminSupprimer() {
		return btnAdminSupprimer;
	}
	public JButton getBtnAdminAjouter() {
		return btnAdminAjouter;
	}
	public JComboBox getCbAdminRights() {
		return cbAdminRights;
	}
	public JButton getBtnModifierMedicament() {
		return btnModifierMedicament;
	}
	public JButton getBtnAjouterMedicament() {
		return btnAjouterMedicament;
	}
	public JButton getBtnModifierRoles() {
		return btnModifierRoles;
	}
	public JPanel getPanelAdminModifMedoc() {
		return panelAdminModifMedoc;
	}
	public JPanel getPanelAdminAjouterMedoc() {
		return panelAdminAjouterMedoc;
	}
	public JLabel getLblMedocAjouterName() {
		return lblMedocAjouterName;
	}
	public JLabel getLblMedocAjouterDanger() {
		return lblMedocAjouterDanger;
	}
	public JTextField getTxtMedocAjouterName() {
		return txtMedocAjouterName;
	}
	public JButton getBtnMedocAjouterAjouter() {
		return btnMedocAjouterAjouter;
	}
	public JComboBox getCbMedocAjouterDanger() {
		return cbMedocAjouterDanger;
	}
	public JComboBox getCbModifMedicamentMedicament() {
		return cbModifMedicamentMedicament;
	}
	public JComboBox getCbModifMedicamentDanger() {
		return cbModifMedicamentDanger;
	}
	public JButton getBtnModifMedicamentModifier() {
		return btnModifMedicamentModifier;
	}
}
