package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Visiteur extends JFrame {

	private JPanel contentPane;
	private JPanel panelVoirTest;
	private JPanel panelInscriptionTest;
	private JPanel panelCard;
	private JScrollPane scrollPaneTests;
	private JTable JTableTests;
	private JLabel lblConnexionStatus;
	private JComboBox cbInscriptionMedicament;
	private JComboBox cbInscriptionMedecin;
	private JComboBox cbInscriptionLaboratory;
	private JPanel panel;
	private JButton btnVoirTestToInscriptionTest;
	private JButton btnInscriptionTestToVoirTest;
	private JPanel panelInscriptionTestCB;
	private JPanel panelInscriptionTestBtn;
	private JLabel lblInscriptionTestLaboAdresse;
	private JLabel lblInscriptionTestLaboRegion;
	private JLabel lblInscriptionTestMedecinNom;
	private JLabel lblInscriptionTestMedecinPrenom;
	private JLabel lblInscriptionTestMedecinPhone;
	private JLabel lblInscriptionTestMedecinEmail;
	private JLabel lblInscriptionTestMedicamentName;
	private JLabel lblInscriptionTestMedicamentDanger;
	private JButton btnInscriptionTestInscription;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visiteur frame = new Visiteur();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Visiteur() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panelCard = new JPanel();
		contentPane.add(panelCard, BorderLayout.NORTH);
		panelCard.setLayout(new CardLayout(0, 0));
		
		panelVoirTest = new JPanel();
		panelVoirTest.setName("panelVoirTest");
		panelCard.add(panelVoirTest, "name_9944042203698");
		panelVoirTest.setLayout(new BoxLayout(panelVoirTest, BoxLayout.X_AXIS));
		
		JTableTests = new JTable();
		JTableTests.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		
		scrollPaneTests = new JScrollPane(JTableTests);
		scrollPaneTests.setName("scrollPaneTests");
		scrollPaneTests.setBounds(5, 223, 800, 300);
		panelVoirTest.add(scrollPaneTests);
		
		panel = new JPanel();
		panelVoirTest.add(panel);
		
		btnVoirTestToInscriptionTest = new JButton("S'inscrire à un test");
		btnVoirTestToInscriptionTest.setName("btnVoirTestToInscriptionTest");
		panel.add(btnVoirTestToInscriptionTest);
		
		panelInscriptionTest = new JPanel();
		panelInscriptionTest.setName("panelInscriptionTest");
		panelCard.add(panelInscriptionTest, "name_9961523916995");
		panelInscriptionTest.setLayout(new BorderLayout(0, 0));
		
		panelInscriptionTestCB = new JPanel();
		panelInscriptionTest.add(panelInscriptionTestCB, BorderLayout.CENTER);
		GridBagLayout gbl_panelInscriptionTestCB = new GridBagLayout();
		gbl_panelInscriptionTestCB.columnWidths = new int[]{219, 71, 88, 42, 0, 0, 0, 0, 0, 0};
		gbl_panelInscriptionTestCB.rowHeights = new int[]{120, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelInscriptionTestCB.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelInscriptionTestCB.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelInscriptionTestCB.setLayout(gbl_panelInscriptionTestCB);
		
		cbInscriptionLaboratory = new JComboBox();
		GridBagConstraints gbc_cbInscriptionLaboratory = new GridBagConstraints();
		gbc_cbInscriptionLaboratory.insets = new Insets(0, 0, 5, 5);
		gbc_cbInscriptionLaboratory.gridx = 0;
		gbc_cbInscriptionLaboratory.gridy = 0;
		panelInscriptionTestCB.add(cbInscriptionLaboratory, gbc_cbInscriptionLaboratory);
		cbInscriptionLaboratory.setName("cbInscriptionLaboratory");
		
		cbInscriptionMedecin = new JComboBox();
		GridBagConstraints gbc_cbInscriptionMedecin = new GridBagConstraints();
		gbc_cbInscriptionMedecin.gridwidth = 4;
		gbc_cbInscriptionMedecin.insets = new Insets(0, 0, 5, 5);
		gbc_cbInscriptionMedecin.gridx = 2;
		gbc_cbInscriptionMedecin.gridy = 0;
		panelInscriptionTestCB.add(cbInscriptionMedecin, gbc_cbInscriptionMedecin);
		cbInscriptionMedecin.setName("cbInscriptionMedecin");
		
		cbInscriptionMedicament = new JComboBox();
		GridBagConstraints gbc_cbInscriptionMedicament = new GridBagConstraints();
		gbc_cbInscriptionMedicament.insets = new Insets(0, 0, 5, 5);
		gbc_cbInscriptionMedicament.gridx = 7;
		gbc_cbInscriptionMedicament.gridy = 0;
		panelInscriptionTestCB.add(cbInscriptionMedicament, gbc_cbInscriptionMedicament);
		cbInscriptionMedicament.setName("cbInscriptionMedicament");
		
		lblInscriptionTestLaboRegion = new JLabel("Region : ");
		GridBagConstraints gbc_lblInscriptionTestLaboRegion = new GridBagConstraints();
		gbc_lblInscriptionTestLaboRegion.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestLaboRegion.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestLaboRegion.gridx = 0;
		gbc_lblInscriptionTestLaboRegion.gridy = 1;
		panelInscriptionTestCB.add(lblInscriptionTestLaboRegion, gbc_lblInscriptionTestLaboRegion);
		lblInscriptionTestLaboRegion.setName("lblInscriptionTestLaboRegion");
		
		lblInscriptionTestMedecinNom = new JLabel("Nom : ");
		GridBagConstraints gbc_lblInscriptionTestMedecinNom = new GridBagConstraints();
		gbc_lblInscriptionTestMedecinNom.gridwidth = 4;
		gbc_lblInscriptionTestMedecinNom.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestMedecinNom.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedecinNom.gridx = 2;
		gbc_lblInscriptionTestMedecinNom.gridy = 1;
		panelInscriptionTestCB.add(lblInscriptionTestMedecinNom, gbc_lblInscriptionTestMedecinNom);
		lblInscriptionTestMedecinNom.setName("lblInscriptionTestMedecinNom");
		
		lblInscriptionTestMedicamentName = new JLabel("Nom : ");
		lblInscriptionTestMedicamentName.setName("lblInscriptionTestMedicamentName");
		GridBagConstraints gbc_lblInscriptionTestMedicamentName = new GridBagConstraints();
		gbc_lblInscriptionTestMedicamentName.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestMedicamentName.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedicamentName.gridx = 7;
		gbc_lblInscriptionTestMedicamentName.gridy = 1;
		panelInscriptionTestCB.add(lblInscriptionTestMedicamentName, gbc_lblInscriptionTestMedicamentName);
		
		lblInscriptionTestLaboAdresse = new JLabel("Adresse : ");
		GridBagConstraints gbc_lblInscriptionTestLaboAdresse = new GridBagConstraints();
		gbc_lblInscriptionTestLaboAdresse.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestLaboAdresse.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestLaboAdresse.gridx = 0;
		gbc_lblInscriptionTestLaboAdresse.gridy = 2;
		panelInscriptionTestCB.add(lblInscriptionTestLaboAdresse, gbc_lblInscriptionTestLaboAdresse);
		lblInscriptionTestLaboAdresse.setName("lblInscriptionTestLaboAdresse");
		
		lblInscriptionTestMedecinPrenom = new JLabel("Prenom : ");
		GridBagConstraints gbc_lblInscriptionTestMedecinPrenom = new GridBagConstraints();
		gbc_lblInscriptionTestMedecinPrenom.gridwidth = 4;
		gbc_lblInscriptionTestMedecinPrenom.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestMedecinPrenom.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedecinPrenom.gridx = 2;
		gbc_lblInscriptionTestMedecinPrenom.gridy = 2;
		panelInscriptionTestCB.add(lblInscriptionTestMedecinPrenom, gbc_lblInscriptionTestMedecinPrenom);
		lblInscriptionTestMedecinPrenom.setName("lblInscriptionTestMedecinPrenom");
		
		lblInscriptionTestMedicamentDanger = new JLabel("Danger potentiel : ");
		lblInscriptionTestMedicamentDanger.setName("lblInscriptionTestMedicamentDanger");
		GridBagConstraints gbc_lblInscriptionTestMedicamentDanger = new GridBagConstraints();
		gbc_lblInscriptionTestMedicamentDanger.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedicamentDanger.gridx = 7;
		gbc_lblInscriptionTestMedicamentDanger.gridy = 2;
		panelInscriptionTestCB.add(lblInscriptionTestMedicamentDanger, gbc_lblInscriptionTestMedicamentDanger);
		
		lblInscriptionTestMedecinPhone = new JLabel("Téléphone : ");
		GridBagConstraints gbc_lblInscriptionTestMedecinPhone = new GridBagConstraints();
		gbc_lblInscriptionTestMedecinPhone.gridwidth = 4;
		gbc_lblInscriptionTestMedecinPhone.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestMedecinPhone.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedecinPhone.gridx = 2;
		gbc_lblInscriptionTestMedecinPhone.gridy = 3;
		panelInscriptionTestCB.add(lblInscriptionTestMedecinPhone, gbc_lblInscriptionTestMedecinPhone);
		lblInscriptionTestMedecinPhone.setName("lblInscriptionTestMedecinPhone");
		
		lblInscriptionTestMedecinEmail = new JLabel("Email : ");
		GridBagConstraints gbc_lblInscriptionTestMedecinEmail = new GridBagConstraints();
		gbc_lblInscriptionTestMedecinEmail.gridwidth = 4;
		gbc_lblInscriptionTestMedecinEmail.anchor = GridBagConstraints.WEST;
		gbc_lblInscriptionTestMedecinEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblInscriptionTestMedecinEmail.gridx = 2;
		gbc_lblInscriptionTestMedecinEmail.gridy = 4;
		panelInscriptionTestCB.add(lblInscriptionTestMedecinEmail, gbc_lblInscriptionTestMedecinEmail);
		lblInscriptionTestMedecinEmail.setName("lblInscriptionTestMedecinEmail");
		
		panelInscriptionTestBtn = new JPanel();
		panelInscriptionTest.add(panelInscriptionTestBtn, BorderLayout.EAST);
		GridBagLayout gbl_panelInscriptionTestBtn = new GridBagLayout();
		gbl_panelInscriptionTestBtn.columnWidths = new int[]{136, 0};
		gbl_panelInscriptionTestBtn.rowHeights = new int[]{25, 0, 0, 0};
		gbl_panelInscriptionTestBtn.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panelInscriptionTestBtn.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelInscriptionTestBtn.setLayout(gbl_panelInscriptionTestBtn);
		
		btnInscriptionTestToVoirTest = new JButton("Voir mes tests");
		GridBagConstraints gbc_btnInscriptionTestToVoirTest = new GridBagConstraints();
		gbc_btnInscriptionTestToVoirTest.insets = new Insets(0, 0, 5, 0);
		gbc_btnInscriptionTestToVoirTest.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnInscriptionTestToVoirTest.gridx = 0;
		gbc_btnInscriptionTestToVoirTest.gridy = 0;
		panelInscriptionTestBtn.add(btnInscriptionTestToVoirTest, gbc_btnInscriptionTestToVoirTest);
		btnInscriptionTestToVoirTest.setName("btnInscriptionTestToVoirTest");
		
		btnInscriptionTestInscription = new JButton("S'inscrire au test");
		btnInscriptionTestInscription.setName("btnInscriptionTestInscription");
		GridBagConstraints gbc_btnInscriptionTestInscription = new GridBagConstraints();
		gbc_btnInscriptionTestInscription.gridx = 0;
		gbc_btnInscriptionTestInscription.gridy = 2;
		panelInscriptionTestBtn.add(btnInscriptionTestInscription, gbc_btnInscriptionTestInscription);
		
		lblConnexionStatus = new JLabel("Connecté en tant que : ");
		lblConnexionStatus.setName("lblConnexionStatus");
		contentPane.add(lblConnexionStatus, BorderLayout.SOUTH);
	}

	public JPanel getPanelVoirTest() {
		return panelVoirTest;
	}
	public JPanel getPanelInscriptionTest() {
		return panelInscriptionTest;
	}
	public JPanel getPanelCard() {
		return panelCard;
	}
	public JTable getJTableTests() {
		return JTableTests;
	}
	public JScrollPane getScrollPaneTests() {
		return scrollPaneTests;
	}
	public JLabel getLblConnexionStatus() {
		return lblConnexionStatus;
	}
	public JComboBox getCbInscriptionMedicament() {
		return cbInscriptionMedicament;
	}
	public JComboBox getCbInscriptionMedecin() {
		return cbInscriptionMedecin;
	}
	public JComboBox getCbInscriptionLaboratory() {
		return cbInscriptionLaboratory;
	}
	public JButton getBtnVoirTestToInscriptionTest() {
		return btnVoirTestToInscriptionTest;
	}
	public JButton getBtnInscriptionTestToVoirTest() {
		return btnInscriptionTestToVoirTest;
	}
	public JLabel getLblInscriptionTestLaboAdresse() {
		return lblInscriptionTestLaboAdresse;
	}
	public JLabel getLblInscriptionTestLaboRegion() {
		return lblInscriptionTestLaboRegion;
	}
	public JLabel getLblInscriptionTestMedecinNom() {
		return lblInscriptionTestMedecinNom;
	}
	public JLabel getLblInscriptionTestMedecinPhone() {
		return lblInscriptionTestMedecinPhone;
	}
	public JLabel getLblInscriptionTestMedecinPrenom() {
		return lblInscriptionTestMedecinPrenom;
	}
	public JLabel getLblInscriptionTestMedecinEmail() {
		return lblInscriptionTestMedecinEmail;
	}
	public JLabel getLblInscriptionTestMedicamentName() {
		return lblInscriptionTestMedicamentName;
	}
	public JLabel getLblInscriptionTestMedicamentDanger() {
		return lblInscriptionTestMedicamentDanger;
	}
	public JButton getBtnInscriptionTestInscription() {
		return btnInscriptionTestInscription;
	}
}
