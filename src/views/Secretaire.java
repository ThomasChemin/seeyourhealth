package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.BoxLayout;

public class Secretaire extends JFrame {

	private JPanel contentPane;
	private JPanel panelFirstConnexion;
	private JLabel lblConnexionStatus;
	private JPanel panelFirstConnexionCB;
	private JComboBox cbFirstConnexionLaboratory;
	private JComboBox cbFirstConnexionMedecin;
	private JLabel lblFirstConnexionLaboRegion;
	private JLabel lblFirstConnexionMedecinNom;
	private JLabel lblFirstConnexionLaboAdresse;
	private JLabel lblFirstConnexionMedecinPrenom;
	private JLabel lblFirstConnexionMedecinPhone;
	private JLabel lblFirstConnexionMedecinEmail;
	private JButton btnFirstConnexionChoose;
	private JPanel panelGererMedecin;
	private JScrollPane scrollPane;
	private JTable jTableGererMedecin;
	private JTable jTableTrierMedicament;
	private JPanel panelGererMedecinBtn;
	private JButton btnGererMedecinModifier;
	private JPanel panelCard;
	private JPanel panel;
	private JLabel lblMedRef;
	private JPanel panelTrierMedicament;
	private JScrollPane scrollPane_1;
	private JTable table;
	private JPanel panel_1;
	private JComboBox cbMedicament;
	private JButton btnTousLesTests;
	private JButton btnTrierParMedicament;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Secretaire frame = new Secretaire();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Secretaire() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 859, 488);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		panelCard = new JPanel();
		contentPane.add(panelCard);
		panelCard.setLayout(new CardLayout(0, 0));
		
		panelFirstConnexion = new JPanel();
		panelFirstConnexion.setName("panelFirstConnexion");
		panelCard.add(panelFirstConnexion, "name_2268988208223");
		panelFirstConnexion.setLayout(new BorderLayout(0, 0));
		
		panelFirstConnexionCB = new JPanel();
		panelFirstConnexionCB.setName("panelFirstConnexionCB");
		panelFirstConnexion.add(panelFirstConnexionCB, BorderLayout.CENTER);
		GridBagLayout gbl_panelFirstConnexionCB = new GridBagLayout();
		gbl_panelFirstConnexionCB.columnWidths = new int[]{104, 104, 104, 104, 0, 104, 104, 104, 104, 0};
		gbl_panelFirstConnexionCB.rowHeights = new int[]{24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelFirstConnexionCB.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panelFirstConnexionCB.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelFirstConnexionCB.setLayout(gbl_panelFirstConnexionCB);
		
		cbFirstConnexionLaboratory = new JComboBox();
		cbFirstConnexionLaboratory.setName("cbFirstConnexionLaboratory");
		GridBagConstraints gbc_cbFirstConnexionLaboratory = new GridBagConstraints();
		gbc_cbFirstConnexionLaboratory.gridwidth = 2;
		gbc_cbFirstConnexionLaboratory.fill = GridBagConstraints.BOTH;
		gbc_cbFirstConnexionLaboratory.insets = new Insets(0, 0, 5, 5);
		gbc_cbFirstConnexionLaboratory.gridx = 1;
		gbc_cbFirstConnexionLaboratory.gridy = 2;
		panelFirstConnexionCB.add(cbFirstConnexionLaboratory, gbc_cbFirstConnexionLaboratory);
		
		cbFirstConnexionMedecin = new JComboBox();
		cbFirstConnexionMedecin.setName("cbFirstConnexionMedecin");
		GridBagConstraints gbc_cbFirstConnexionMedecin = new GridBagConstraints();
		gbc_cbFirstConnexionMedecin.gridwidth = 2;
		gbc_cbFirstConnexionMedecin.fill = GridBagConstraints.BOTH;
		gbc_cbFirstConnexionMedecin.insets = new Insets(0, 0, 5, 5);
		gbc_cbFirstConnexionMedecin.gridx = 5;
		gbc_cbFirstConnexionMedecin.gridy = 2;
		panelFirstConnexionCB.add(cbFirstConnexionMedecin, gbc_cbFirstConnexionMedecin);
		
		lblFirstConnexionLaboRegion = new JLabel("Region : ");
		lblFirstConnexionLaboRegion.setName("lblFirstConnexionLaboRegion");
		GridBagConstraints gbc_lblFirstConnexionLaboRegion = new GridBagConstraints();
		gbc_lblFirstConnexionLaboRegion.gridwidth = 2;
		gbc_lblFirstConnexionLaboRegion.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionLaboRegion.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionLaboRegion.gridx = 1;
		gbc_lblFirstConnexionLaboRegion.gridy = 4;
		panelFirstConnexionCB.add(lblFirstConnexionLaboRegion, gbc_lblFirstConnexionLaboRegion);
		
		lblFirstConnexionMedecinNom = new JLabel("Nom : ");
		lblFirstConnexionMedecinNom.setName("lblFirstConnexionMedecinNom");
		GridBagConstraints gbc_lblFirstConnexionMedecinNom = new GridBagConstraints();
		gbc_lblFirstConnexionMedecinNom.gridwidth = 2;
		gbc_lblFirstConnexionMedecinNom.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionMedecinNom.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionMedecinNom.gridx = 5;
		gbc_lblFirstConnexionMedecinNom.gridy = 4;
		panelFirstConnexionCB.add(lblFirstConnexionMedecinNom, gbc_lblFirstConnexionMedecinNom);
		
		lblFirstConnexionLaboAdresse = new JLabel("Adresse : ");
		lblFirstConnexionLaboAdresse.setName("lblFirstConnexionLaboAdresse");
		GridBagConstraints gbc_lblFirstConnexionLaboAdresse = new GridBagConstraints();
		gbc_lblFirstConnexionLaboAdresse.gridwidth = 2;
		gbc_lblFirstConnexionLaboAdresse.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionLaboAdresse.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionLaboAdresse.gridx = 1;
		gbc_lblFirstConnexionLaboAdresse.gridy = 5;
		panelFirstConnexionCB.add(lblFirstConnexionLaboAdresse, gbc_lblFirstConnexionLaboAdresse);
		
		lblFirstConnexionMedecinPrenom = new JLabel("Prenom : ");
		lblFirstConnexionMedecinPrenom.setName("lblFirstConnexionMedecinPrenom");
		GridBagConstraints gbc_lblFirstConnexionMedecinPrenom = new GridBagConstraints();
		gbc_lblFirstConnexionMedecinPrenom.gridwidth = 2;
		gbc_lblFirstConnexionMedecinPrenom.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionMedecinPrenom.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionMedecinPrenom.gridx = 5;
		gbc_lblFirstConnexionMedecinPrenom.gridy = 5;
		panelFirstConnexionCB.add(lblFirstConnexionMedecinPrenom, gbc_lblFirstConnexionMedecinPrenom);
		
		lblFirstConnexionMedecinPhone = new JLabel("Téléphone : ");
		lblFirstConnexionMedecinPhone.setName("lblFirstConnexionMedecinPhone");
		GridBagConstraints gbc_lblFirstConnexionMedecinPhone = new GridBagConstraints();
		gbc_lblFirstConnexionMedecinPhone.gridwidth = 2;
		gbc_lblFirstConnexionMedecinPhone.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionMedecinPhone.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionMedecinPhone.gridx = 5;
		gbc_lblFirstConnexionMedecinPhone.gridy = 6;
		panelFirstConnexionCB.add(lblFirstConnexionMedecinPhone, gbc_lblFirstConnexionMedecinPhone);
		
		lblFirstConnexionMedecinEmail = new JLabel("Email : ");
		lblFirstConnexionMedecinEmail.setName("lblFirstConnexionMedecinEmail");
		GridBagConstraints gbc_lblFirstConnexionMedecinEmail = new GridBagConstraints();
		gbc_lblFirstConnexionMedecinEmail.gridwidth = 2;
		gbc_lblFirstConnexionMedecinEmail.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstConnexionMedecinEmail.fill = GridBagConstraints.BOTH;
		gbc_lblFirstConnexionMedecinEmail.gridx = 5;
		gbc_lblFirstConnexionMedecinEmail.gridy = 7;
		panelFirstConnexionCB.add(lblFirstConnexionMedecinEmail, gbc_lblFirstConnexionMedecinEmail);
		
		btnFirstConnexionChoose = new JButton("Choisir ce médecin");
		btnFirstConnexionChoose.setName("btnFirstConnexionChoose");
		GridBagConstraints gbc_btnFirstConnexionChoose = new GridBagConstraints();
		gbc_btnFirstConnexionChoose.insets = new Insets(0, 0, 0, 5);
		gbc_btnFirstConnexionChoose.gridx = 4;
		gbc_btnFirstConnexionChoose.gridy = 12;
		panelFirstConnexionCB.add(btnFirstConnexionChoose, gbc_btnFirstConnexionChoose);
		
		panelGererMedecin = new JPanel();
		panelGererMedecin.setName("panelGererMedecin");
		panelCard.add(panelGererMedecin, "name_6049105671410");
		panelGererMedecin.setLayout(new BoxLayout(panelGererMedecin, BoxLayout.X_AXIS));
		
		scrollPane = new JScrollPane();
		panelGererMedecin.add(scrollPane);
		
		jTableGererMedecin = new JTable();
		jTableGererMedecin.setRowHeight(20);
		jTableGererMedecin.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		jTableGererMedecin.setName("jTableGererMedecin");
		scrollPane.setViewportView(jTableGererMedecin);
		
		panelGererMedecinBtn = new JPanel();
		panelGererMedecinBtn.setName("panelGererMedecinBtn");
		panelGererMedecin.add(panelGererMedecinBtn);
		
		btnGererMedecinModifier = new JButton("Modifier");
		btnGererMedecinModifier.setName("btnGererMedecinModifier");
		panelGererMedecinBtn.add(btnGererMedecinModifier);
		
		panelTrierMedicament = new JPanel();
		panelTrierMedicament.setName("panelTrierMedicament");
		panelCard.add(panelTrierMedicament, "name_1997651102499");
		panelTrierMedicament.setLayout(new BoxLayout(panelTrierMedicament, BoxLayout.X_AXIS));
		
		scrollPane_1 = new JScrollPane();
		panelTrierMedicament.add(scrollPane_1);
		
		jTableTrierMedicament = new JTable();
		jTableTrierMedicament.setRowHeight(20);
		jTableTrierMedicament.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		jTableTrierMedicament.setName("jTableTrierMedicament");
		scrollPane_1.setViewportView(jTableTrierMedicament);
		
		panel_1 = new JPanel();
		panelTrierMedicament.add(panel_1);
		
		cbMedicament = new JComboBox();
		cbMedicament.setName("cbMedicament");
		panel_1.add(cbMedicament);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{336, 167, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{15, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblConnexionStatus = new JLabel("Connecté en tant que : ");
		GridBagConstraints gbc_lblConnexionStatus = new GridBagConstraints();
		gbc_lblConnexionStatus.insets = new Insets(0, 0, 5, 5);
		gbc_lblConnexionStatus.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblConnexionStatus.gridx = 0;
		gbc_lblConnexionStatus.gridy = 0;
		panel.add(lblConnexionStatus, gbc_lblConnexionStatus);
		lblConnexionStatus.setName("lblConnexionStatus");
		
		lblMedRef = new JLabel("Médecin référent : ");
		lblMedRef.setName("lblMedRef");
		GridBagConstraints gbc_lblMedRef = new GridBagConstraints();
		gbc_lblMedRef.insets = new Insets(0, 0, 5, 0);
		gbc_lblMedRef.gridx = 6;
		gbc_lblMedRef.gridy = 0;
		panel.add(lblMedRef, gbc_lblMedRef);
		
		btnTousLesTests = new JButton("Tous les tests");
		btnTousLesTests.setName("btnAllTests");
		GridBagConstraints gbc_btnTousLesTests = new GridBagConstraints();
		gbc_btnTousLesTests.insets = new Insets(0, 0, 0, 5);
		gbc_btnTousLesTests.gridx = 0;
		gbc_btnTousLesTests.gridy = 1;
		panel.add(btnTousLesTests, gbc_btnTousLesTests);
		
		btnTrierParMedicament = new JButton("Trier par médicament");
		btnTrierParMedicament.setName("btnTrie");
		GridBagConstraints gbc_btnTrierParMedicament = new GridBagConstraints();
		gbc_btnTrierParMedicament.insets = new Insets(0, 0, 0, 5);
		gbc_btnTrierParMedicament.gridx = 1;
		gbc_btnTrierParMedicament.gridy = 1;
		panel.add(btnTrierParMedicament, gbc_btnTrierParMedicament);
	}

	public JPanel getPanelFirstConnexion() {
		return panelFirstConnexion;
	}
	public JComboBox getCbFirstConnexionLaboratory() {
		return cbFirstConnexionLaboratory;
	}
	public JLabel getLblFirstConnexionLaboRegion() {
		return lblFirstConnexionLaboRegion;
	}
	public JLabel getLblFirstConnexionLaboAdresse() {
		return lblFirstConnexionLaboAdresse;
	}
	public JComboBox getCbFirstConnexionMedecin() {
		return cbFirstConnexionMedecin;
	}
	public JLabel getLblFirstConnexionMedecinNom() {
		return lblFirstConnexionMedecinNom;
	}
	public JLabel getLblFirstConnexionMedecinPrenom() {
		return lblFirstConnexionMedecinPrenom;
	}
	public JLabel getLblFirstConnexionMedecinPhone() {
		return lblFirstConnexionMedecinPhone;
	}
	public JLabel getLblFirstConnexionMedecinEmail() {
		return lblFirstConnexionMedecinEmail;
	}
	public JButton getBtnFirstConnexionChoose() {
		return btnFirstConnexionChoose;
	}
	public JLabel getLblConnexionStatus() {
		return lblConnexionStatus;
	}
	public JPanel getPanelGererMedecin() {
		return panelGererMedecin;
	}
	public JTable getJTableGererMedecin() {
		return jTableGererMedecin;
	}
	public JPanel getPanelCard() {
		return panelCard;
	}
	public JButton getBtnGererMedecinModifier() {
		return btnGererMedecinModifier;
	}
	public JLabel getLblMedRef() {
		return lblMedRef;
	}
	public JPanel getPanelTrierMedicament() {
		return panelTrierMedicament;
	}
	public JTable getJTableTrierMedicament() {
		return jTableTrierMedicament;
	}
	public JButton getBtnTousLesTests() {
		return btnTousLesTests;
	}
	public JButton getBtnTrierParMdicament() {
		return btnTrierParMedicament;
	}
	public JComboBox getCbMedicament() {
		return cbMedicament;
	}
}
