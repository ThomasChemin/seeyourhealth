package DefaultModelJTable;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import model.Test;

public class MyDefaultModelVisiteur extends DefaultTableModel{

	private HashSet<Test> modifiedTests = new HashSet<Test>();
	private List<Test> tests;
	private String[] columnNames = {"Numéro", 
			"Nom du médecin",
			"Nom du médicament",
			"Résultat",
	};

	@Override
	public String toString() {
		return "MyDefaultModelVisiteur [tests=" + tests + "]";
	}

	public MyDefaultModelVisiteur(List<Test> tests) {
		this.tests = tests;
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return tests == null ? 0 : tests.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		switch (column) {
		case 0:
			return row+1;
		case 1:
			return tests.get(row).getMedecin().getLastName() + " " + tests.get(row).getMedecin().getFirstName();
		case 2:
			return tests.get(row).getSample().getName();
		case 3:
			return tests.get(row).getResult();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		switch (columnIndex) {
		case 0:
			type = Integer.class;
			break;
		case 1:
		case 2:
		case 3:
			type = String.class;
			break;
		}
		return type;
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		switch (column) {
		case 3:
			tests.get(row).setResult((String)value);
			break;
		}
		modifiedTests.add(tests.get(row));
	}

	public HashSet<Test> getModifiedTests() {
		return modifiedTests;
	}

	public void setModifiedTests(HashSet<Test> modifiedTests) {
		this.modifiedTests = modifiedTests;
	}

	public List<Test> getTests() {
		return tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}	



}
