package DefaultModelJTable;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Rights;
import model.Test;
import model.UserRight;

public class MyDefaultModelAdminUsers extends DefaultTableModel{

	private HashSet<UserRight> modifiedRights = new HashSet<UserRight>();
	private List<UserRight> userRights;
	private String[] columnNames = {"Numéro", 
			"Nom du patient",
			"Droit",
	};

	@Override
	public String toString() {
		return null;
	}

	public MyDefaultModelAdminUsers(List<UserRight> userRights) {
		this.userRights = userRights;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {
		return userRights == null ? 0 : userRights.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		switch (column) {
		case 0:
			return row+1;
		case 1:
			return userRights.get(row).getUser().getLastName() + " " + userRights.get(row).getUser().getFirstName();
		case 2:
			return userRights.get(row).getRight().getName();
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		switch (columnIndex) {
		case 0:
			type = Integer.class;
			break;
		case 1:
		case 2:
			type = String.class;
			break;
		}
		return type;
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		switch (column) {
		case 2:
			userRights.get(row).setRight((Rights)value);
			break;
		}
		modifiedRights.add(userRights.get(row));
	}

	public HashSet<UserRight> getModifiedUserRights() {
		return modifiedRights;
	}

	public void setModifiedUserRights(HashSet<UserRight> modifiedUserRights) {
		this.modifiedRights = modifiedRights;
	}

	public List<UserRight> getUserRights() {
		return userRights;
	}

	public void setUserRights(List<UserRight> userRights) {
		this.userRights = userRights;
	}	



}
