package DAO;

import java.sql.PreparedStatement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.query.spi.HQLQueryPlan;
import org.hibernate.metamodel.source.annotations.entity.EntityClass;

import model.Medecin;

/**
 * DAOMedecin for the access of the table Medecin in the database
 * @author student
 *
 */
public class DAOMedecin extends DAOGeneric<Medecin>{

	private Session session;
	
	public DAOMedecin(Session session) {
		super(session, Medecin.class);
	}
	
	/**
	 * Find all Medecin from a laboratory
	 * @param laboratoryId : the id of the Laboratory
	 * @return meds : List Medecin
	 */
	public List<Medecin> findAllByLaboratoryId(int laboratoryId){
		String sql = "SELECT * FROM Medecin WHERE laboratory = :laboratory";
		SQLQuery query = session.createSQLQuery(sql);
		query.setInteger("laboratory", laboratoryId);
		query.addEntity(entityClass);
		List<Medecin> meds = query.list();
		return meds;
	}

}
