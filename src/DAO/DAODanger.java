package DAO;

import java.sql.PreparedStatement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.query.spi.HQLQueryPlan;
import org.hibernate.metamodel.source.annotations.entity.EntityClass;

import model.Danger;

/**
 * DAODanger for the access of the table Danger in the database
 * @author student
 *
 */
public class DAODanger extends DAOGeneric<Danger>{

	public DAODanger(Session session) {
		super(session, Danger.class);
	}

}
