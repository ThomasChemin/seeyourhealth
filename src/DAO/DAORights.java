package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import model.Rights;
import model.Users;

/**
 * DAORights for the access of the table Rights in the database
 * @author student
 *
 */
public class DAORights extends DAOGeneric<Rights>{

	public DAORights(Session session) {
		super(session, Rights.class);
	}
	
	/**
	 * Find a Right by his name
	 * @param name : the name of the right
	 * @return r : Right
	 */
	public Rights findByName(String name) {
		String sql = "SELECT * FROM Rights WHERE name=:name";
		SQLQuery query = session.createSQLQuery(sql);
		query.setString("name", name);
		query.addEntity(entityClass);
		Rights r = (Rights) query.uniqueResult();
		return r;
	}

}
