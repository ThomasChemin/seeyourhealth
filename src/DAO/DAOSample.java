package DAO;

import org.hibernate.Session;

import model.Sample;

/**
 * DAOSample for the access of the table Sample in the database
 * @author student
 *
 */
public class DAOSample extends DAOGeneric<Sample>{

	public DAOSample(Session session) {
		super(session, Sample.class);
	}

}
