package DAO;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import model.Users;
import util.hashPassword;

/**
 * DAOUser for the access at the Database for the table User
 * @author student
 *
 */
public class DAOUser extends DAOGeneric<Users>{

	public DAOUser(Session session) {
		super(session, Users.class);
	}
	
	/**
	 * Find a user with his email and password
	 * @param email email of the user
	 * @param password password of the user
	 * @return u : a user
	 * @throws NoSuchAlgorithmException : throw when a particular cryptographic algorithm is requested but is not available in the environment.
	 */
	public Users findByMailAndPassword(String email, String password) throws NoSuchAlgorithmException{
		String sql = "SELECT * FROM Users WHERE email = :email AND password = :password";
		SQLQuery query = session.createSQLQuery(sql);
		query.setString("email", email);
		query.setString("password", password);
		query.addEntity(entityClass);
		Users u = (Users) query.uniqueResult();
		return u;
	}

}