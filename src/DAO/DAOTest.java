package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import model.Test;

/**
 * DAOTest for the access of the table Test in the database
 * @author student
 *
 */
public class DAOTest extends DAOGeneric<Test>{

	public DAOTest(Session session) {
		super(session, Test.class);
	}
	
	/**
	 * Find all Tests from a User by his id
	 * @param idUser the id of the user
	 * @return tests : List Test
	 */
	public List<Test> findAllByUserId(int idUser){
		String sql = "SELECT * FROM Test WHERE idUser = :idUser";
		SQLQuery query = session.createSQLQuery(sql);
		query.setInteger("idUser", idUser);
		query.addEntity(entityClass);
		List<Test> tests = query.list();
		return tests;
	}
	
	/**
	 * Find all Tests from a Medecin by his id
	 * @param idMedecin the id of the user
	 * @return tests : List Test
	 */
	public List<Test> findAllByMedecinId(int idMedecin){
		String sql = "SELECT * FROM Test WHERE idMedRef = :idMedecin";
		SQLQuery query = session.createSQLQuery(sql);
		query.setInteger("idMedecin", idMedecin);
		query.addEntity(entityClass);
		List<Test> tests = query.list();
		return tests;
	}

}
