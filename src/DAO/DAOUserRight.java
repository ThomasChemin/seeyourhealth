package DAO;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import model.UserRight;

/**
 * DAOUserRight for the access of the table UserRight in the database
 * @author student
 *
 */
public class DAOUserRight extends DAOGeneric<UserRight>{

	public DAOUserRight(Session session) {
		super(session, UserRight.class);
	}
	
	/**
	 * Return a list of the rights of a user
	 * @param idUser the id of the user
	 * @return ur : List of userRight of the user
	 */
	public List<UserRight> findAllByUserId(int idUser){
		String sql = "SELECT * FROM UserRight WHERE idUser = :idUser";
		SQLQuery query = session.createSQLQuery(sql);
		query.setInteger("idUser", idUser);
		query.addEntity(entityClass);
		List<UserRight> ur = query.list();
		return ur;
	}

}
