package DAO;

import java.sql.PreparedStatement;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import model.Danger;
import model.Laboratory;
import model.Medecin;
import model.Rights;
import model.Sample;
import model.Test;
import model.UserRight;
import model.Users;

/**
 * DAOGeneric for the access of the table T in the database
 * @author student
 *
 * @param <T> The class of the DAO
 */
public abstract class DAOGeneric<T> {

	protected Session session;
	protected Class<T> entityClass;

	public DAOGeneric(Session session, Class<T> entityClass) {
		this.session = session;
		this.entityClass = entityClass;
	}

	/**
	 * Find an entity by his id
	 * @param id the id of the table T
	 * @return T 
	 */
	public T find(int id) {
		T entity = (T)session.get(entityClass, id);
		return entity;
	}

	/**
	 * Insert or Update the specified entity
	 * @param entity the entity that need to be saved or updated
	 */
	public void saveOrUpdate(T entity) {
		session.beginTransaction();
		session.saveOrUpdate(entity);
		session.getTransaction().commit();
	}

	/**
	 * Delete the specified entity from the database
	 * @param entity the entity that need to be deleted
	 */
	public void delete(T entity) {
		session.beginTransaction();
		session.delete(entity);
		session.getTransaction().commit();
	}

	/**
	 * return all entities
	 * @return List T
	 */
	public List<T> findAll() {
		List<T> entities = session.createCriteria(entityClass).list();
		return entities;
	}
	
	/**
	 * Count the number of entities in the DB
	 * @return int
	 */
	public int count() {
		int c = ((Number) session.createCriteria(entityClass).setProjection(Projections.rowCount()).uniqueResult()).intValue();
		return c;
	}

	/**
	 * Commit a transaction that failed to commit
	 */
	public void resetTransaction() {
		session.getTransaction().commit();
	}
	
	/**
	 * close the current session
	 */
	public void closeSession() {
		session.close();
	}
	
}
