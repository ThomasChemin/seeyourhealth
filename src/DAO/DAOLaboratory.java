package DAO;

import org.hibernate.Session;

import model.Laboratory;

/**
 * DAOLaboratory for the access of the table Laboratory in the database
 * @author student
 *
 */
public class DAOLaboratory extends DAOGeneric<Laboratory>{

	public DAOLaboratory(Session session) {
		super(session, Laboratory.class);
	}

}
